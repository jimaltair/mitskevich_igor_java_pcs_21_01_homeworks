-- Выводит информацию о каждом заказе: его номер, кто его сформировал (имя пользователя) и его стоимость
-- (сумма произведений количества заказанных товаров и их цены), в отсортированном по номеру заказа виде.
-- Последнему столбцу присвоено название Стоимость.

select order_item_id, name_client, sum(order_item.amount * price) as Стоимость
from client
         inner join order_item using (client_id)
         inner join item using (item_id)
group by order_item_id, name_client
order by order_item_id;

-- Для каждого отдельного товара выводит информацию о количестве проданных экземпляров и
-- их стоимости за текущий год. Вычисляемые столбцы называются Количество и Сумма.
-- Информация отсортирована по убыванию стоимости.

select name_item, sum(order_item.amount) as Количество, sum(price * order_item.amount) as Сумма
from bill
         inner join order_item using (order_item_id)
         inner join item using (item_id)
where createdAT between '2021-01-01' and '2021-12-31'
group by name_item
order by Сумма desc;