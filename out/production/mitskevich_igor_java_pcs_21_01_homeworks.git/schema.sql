CREATE TABLE item (
                      item_id BIGSERIAL NOT NULL,
                      name_item VARCHAR(50),
                      price DECIMAL(8,2),
                      amount INTEGER
);


ALTER TABLE item ADD CONSTRAINT item_pkey PRIMARY KEY (item_id);

CREATE TABLE client (
                        client_id BIGSERIAL NOT NULL,
                        name_client VARCHAR(50),
                        email VARCHAR(50),
                        mobile VARCHAR(15)
);


ALTER TABLE client ADD CONSTRAINT client_pkey PRIMARY KEY (client_id);

CREATE TABLE order_item (
                            order_item_id BIGSERIAL NOT NULL,
                            client_id INTEGER NOT NULL,
                            item_id INTEGER NOT NULL,
                            amount INTEGER,
                            status VARCHAR(30)
);


ALTER TABLE order_item ADD CONSTRAINT order_item_pkey PRIMARY KEY (order_item_id);

CREATE TABLE item_booking (
                              item_booking_id BIGSERIAL NOT NULL,
                              item_id INTEGER NOT NULL,
                              amount INTEGER,
                              date_item_booking_beg DATE,
                              date_item_booking_end DATE
);


ALTER TABLE item_booking ADD CONSTRAINT item_booking_pkey PRIMARY KEY (item_booking_id);

CREATE TABLE order_booking (
                               order_booking_id BIGSERIAL NOT NULL,
                               order_item_id INTEGER NOT NULL,
                               date_order_booking_beg DATE,
                               date_order_booking_end DATE
);


ALTER TABLE order_booking ADD CONSTRAINT order_booking_pkey PRIMARY KEY (order_booking_id);

CREATE TABLE bill (
                      bill_id BIGSERIAL NOT NULL,
                      order_item_id INTEGER NOT NULL,
                      sum DECIMAL(9,2),
                      createdAt DATE
);


ALTER TABLE bill ADD CONSTRAINT bill_pkey PRIMARY KEY (bill_id);

ALTER TABLE order_item ADD CONSTRAINT order_item_client_id_fkey FOREIGN KEY (client_id) REFERENCES client(client_id);
ALTER TABLE order_item ADD CONSTRAINT order_item_item_id_fkey FOREIGN KEY (item_id) REFERENCES item(item_id);
ALTER TABLE item_booking ADD CONSTRAINT item_booking_item_id_fkey FOREIGN KEY (item_id) REFERENCES item(item_id);
ALTER TABLE order_booking ADD CONSTRAINT order_booking_order_item_id_fkey FOREIGN KEY (order_item_id) REFERENCES order_item(order_item_id);
ALTER TABLE bill ADD CONSTRAINT bill_order_item_id_fkey FOREIGN KEY (order_item_id) REFERENCES order_item(order_item_id);