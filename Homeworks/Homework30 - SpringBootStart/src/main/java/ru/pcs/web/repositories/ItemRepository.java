package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Item;

public interface ItemRepository extends JpaRepository<Item, Long> {
}