package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.AddItemForm;
import ru.pcs.web.models.Item;
import ru.pcs.web.repositories.ItemRepository;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;

    @Override
    public void addItem(AddItemForm form) {
        Item item = Item.builder()
                .name(form.getName())
                .description(form.getDescription())
                .price(form.getPrice())
                .createdAt(LocalDateTime.now().withNano(0))
                .build();
        itemRepository.save(item);
    }
}