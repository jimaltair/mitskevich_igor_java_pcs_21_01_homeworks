package ru.pcs.web.services;

import ru.pcs.web.dto.SignUpForm;

/**
 * 08.11.2021
 * Homework30 - SpringBootStart
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface SignUpService {
    void signUp(SignUpForm form);
}
