package ru.pcs.web.services;

import ru.pcs.web.dto.AddItemForm;

public interface ItemService {
    void addItem(AddItemForm form);
}