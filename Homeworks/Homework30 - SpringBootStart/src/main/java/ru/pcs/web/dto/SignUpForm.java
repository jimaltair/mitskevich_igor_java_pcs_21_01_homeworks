package ru.pcs.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.validation.NotSameNames;
import ru.pcs.web.validation.ValidPassword;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 08.11.2021
 * Homework30 - SpringBootStart
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@NotSameNames
public class SignUpForm {
    @Size(min = 4, max =  20)
    @NotBlank
    private String firstName;

    @Size(min = 4, max =  20)
    @NotBlank
    private String lastName;

    @NotBlank
    @ValidPassword
    private String password;

    @Email
    @NotBlank
    private String email;
}
