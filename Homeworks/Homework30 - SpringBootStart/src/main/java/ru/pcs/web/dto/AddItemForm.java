package ru.pcs.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddItemForm {

    @Size(min = 4, max = 20)
    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @PositiveOrZero
    @NotNull
    private Double price;
}
