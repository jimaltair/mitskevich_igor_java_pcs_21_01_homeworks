package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.pcs.web.dto.AddItemForm;
import ru.pcs.web.services.ItemService;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/addItem")
public class AddItemController {

    private final ItemService itemService;

    @RequestMapping()
    public String getAddItemPage(Model model) {
        model.addAttribute("addItemForm", new AddItemForm());
        return "addItem";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String addItem(@Valid AddItemForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("addItemForm", form);
            return "addItem";
        }
        itemService.addItem(form);
        return "redirect:/addItem";
    }
}
