package ru.pcs.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 10.11.2021
 * Homework31 - SpringBootSecurity
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Controller
@RequestMapping("/profile")
public class ProfileController {
    @GetMapping
    public String getProfilePage() {
        return "profile";
    }
}
