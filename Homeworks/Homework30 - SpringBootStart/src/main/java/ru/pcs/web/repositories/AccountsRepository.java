package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 08.11.2021
 * Homework30 - SpringBootStart
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface AccountsRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByEmail(String email);

    List<Account> findAllByState(Account.State state);
}
