package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDto;

import java.util.List;

/**
 * 10.11.2021
 * Homework31 - SpringBootSecurity
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface AccountsService {
    List<AccountDto> getAllAccounts();
    void deleteAccount(Long accountId);
}
