package ru.pcs.web;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import ru.pcs.web.models.Account;
import ru.pcs.web.security.JwtTokenManager;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 16.11.2021
 * Homework33 - JWT
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@SpringBootTest
class JwtTokenManagerTests {

    private JwtTokenManager jwtTokenManager;

    @BeforeEach
    void beforeEach() {
        jwtTokenManager = new JwtTokenManager();
        jwtTokenManager.setSECRET_KEY("secret_key");
    }

    @Test
    void createAccountToken() {

        String expected = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicGFzc3dvcmQiOiIxMjM0NSIsInJvbGUiOiJBRE1JTiIsInN0YXRlIjoiQ09ORklSTUVEIiwiZXhwIjoxNjM4MjIzMjAwLCJlbWFpbCI6ImkubWl0c2tldmljaEBtYWlsLnJ1In0.kYhBsSJRM1poNH8uNr_m0DvislG3DGLc6_2Sx-gVbP0";

        Account account = new Account();
        account.setId(1L);
        account.setRole(Account.Role.valueOf("ADMIN"));
        account.setState(Account.State.valueOf("CONFIRMED"));
        account.setEmail("i.mitskevich@mail.ru");
        account.setPassword("12345");

        String result = jwtTokenManager.createAccountToken(account);
        assertThat(result).isEqualTo(expected);
    }
}
