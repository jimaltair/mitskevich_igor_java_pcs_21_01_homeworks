package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.models.Account;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 19.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@SpringBootTest
@RequiredArgsConstructor
class AccountsServiceImplTest {

    @Autowired
    private AccountsService accountsService;

    @Test
    void getAccountByRefreshToken() {
        String refreshToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicGFzc3dvcmQiOiIkMmEkMTAkTzdYWFdMOS9LU3RjUW50eTVGcEczdVZ4U1N1YWVTd1JyRWljd2guUS5IVE52Qlgvbk1waGkiLCJyb2xlIjoiQURNSU4iLCJzdGF0ZSI6IkNPTkZJUk1FRCIsImV4cCI6MTY2ODgwODgwMCwiZW1haWwiOiJpLm1pdHNrZXZpY2hAbWFpbC5ydSJ9.SLf_SiG-_GEExvv8THvKW4_2Az6NZltZgFQBjDbhE3U";

        Account account = new Account();
        account.setId(1L);
        account.setRole(Account.Role.valueOf("ADMIN"));
        account.setState(Account.State.valueOf("CONFIRMED"));
        account.setEmail("i.mitskevich@mail.ru");
        account.setPassword("$2a$10$O7XXWL9/KStcQnty5FpG3uVxSSuaeSwRrEicwh.Q.HTNvBX/nMphi");
        account.setToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicGFzc3dvcmQiOiIkMmEkMTAkTzdYWFdMOS9LU3RjUW50eTVGcEczdVZ4U1N1YWVTd1JyRWljd2guUS5IVE52Qlgvbk1waGkiLCJyb2xlIjoiQURNSU4iLCJzdGF0ZSI6IkNPTkZJUk1FRCIsImV4cCI6MTYzODQ4MjQwMCwiZW1haWwiOiJpLm1pdHNrZXZpY2hAbWFpbC5ydSJ9.GieDrLgIdg4b3oWCAuvFZUjuLUA0shgodjTYIogAojw");
        account.setRefreshToken(refreshToken);

        AccountDto expected = AccountDto.from(account);

        AccountDto result = accountsService.getAccountByRefreshToken(refreshToken);

        assertThat(result).isEqualTo(expected);
    }
}