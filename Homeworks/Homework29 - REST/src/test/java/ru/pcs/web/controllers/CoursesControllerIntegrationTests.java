package ru.pcs.web.controllers;

import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.transaction.annotation.Transactional;
import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.CoursesResponse;
import ru.pcs.web.models.Course;
import ru.pcs.web.repositories.CoursesRepository;
import ru.pcs.web.services.CoursesService;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/**
 * 23.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Slf4j
@SpringBootTest
@AutoConfigureEmbeddedDatabase(type = AutoConfigureEmbeddedDatabase.DatabaseType.POSTGRES,
        provider = AutoConfigureEmbeddedDatabase.DatabaseProvider.ZONKY,
        refresh = AutoConfigureEmbeddedDatabase.RefreshMode.BEFORE_EACH_TEST_METHOD)
@Transactional
@Sql(value = {"classpath:integration_tests_data.sql"})
class CoursesControllerIntegrationTests {

    @Autowired
    CoursesController coursesController;

    @Autowired
    CoursesService coursesService;

    @Test
    void service_return_correct_counts_of_courses() {
        assertThat(coursesService.getCourses(0, 10).size(), is(equalTo(3)));
    }

    @Test
    void return_correct_courses_when_getCourses() {
        ResponseEntity<CoursesResponse> actual = coursesController.getCourses(0, 10);
        assertThat(actual, is(equalTo(expected_courses())));
    }

    public ResponseEntity<CoursesResponse> expected_courses() {
        List<CourseDto> coursesList = new ArrayList<>();
        CourseDto course1 = CourseDto.builder()
                .id(1L)
                .title("Java")
                .build();
        CourseDto course2 = CourseDto.builder()
                .id(2L)
                .title("SQL")
                .build();
        CourseDto course3 = CourseDto.builder()
                .id(3L)
                .title("Spring")
                .build();
        Collections.addAll(coursesList, course1, course2, course3);

        return ResponseEntity.ok().body(CoursesResponse.builder().data(coursesList).build());
    }

    @Test
    void return_correct_course_when_deleteExistedCourse() {
        ResponseEntity<CourseDto> actual = coursesController.deleteCourse(1L);
        ResponseEntity<CourseDto> expected = ResponseEntity.status(202).body(CourseDto.builder()
                .id(1L)
                .title("Java")
                .build());
        assertThat(actual, is(equalTo(expected)));
    }
}
