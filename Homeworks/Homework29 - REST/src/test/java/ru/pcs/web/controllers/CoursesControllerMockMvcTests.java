package ru.pcs.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.models.Course;
import ru.pcs.web.services.CoursesService;

import javax.persistence.EntityNotFoundException;
import javax.xml.ws.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static java.lang.System.out;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * 20.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class CoursesControllerMockMvcTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CoursesService coursesService;

    @BeforeEach
    void setUp() {
        List<CourseDto> resultList = new ArrayList<>();
        resultList.add(new CourseDto(1L, "test_title1"));
        resultList.add(new CourseDto(2L, "test_title2"));
        resultList.add(new CourseDto(3L, "test_title3"));

        CourseDto testCourse = new CourseDto(1L, "TEST");

        when(coursesService.getCourses(0, 3)).thenReturn(resultList);
        when(coursesService.getCourse(Mockito.any())).thenReturn(testCourse);
        when(coursesService.addCourse(new CourseDto(null, "TEST")))
                .thenReturn(new CourseDto(1L, "TEST_CREATED"));
        when(coursesService.updateCourse(1L, new CourseDto(null, "TEST")))
                .thenReturn(new CourseDto(1L, "TEST_UPDATED"));
        when(coursesService.deleteCourse(1L)).thenReturn(new CourseDto(1L, "TEST_DELETED"));
        when(coursesService.deleteCourse(100L)).thenThrow(EntityNotFoundException.class).thenReturn(null);
    }

    @Test
    void givenCourses_whenGetCourses_thenStatus200andGettingReturns() throws Exception {
        List<CourseDto> resultList = new ArrayList<>();
        resultList.add(new CourseDto(1L, "test_title1"));
        resultList.add(new CourseDto(2L, "test_title2"));
        resultList.add(new CourseDto(3L, "test_title3"));

        mockMvc.perform(get("/courses?page={0}&size={1}", 0, 3))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("data[0].id", is(1)))
                .andExpect(jsonPath("data[0].title", is("test_title1")))
                .andExpect(jsonPath("data[1].id", is(2)))
                .andExpect(jsonPath("data[1].title", is("test_title2")))
                .andExpect(jsonPath("data[2].id", is(3)))
                .andExpect(jsonPath("data[2].title", is("test_title3")));
    }

    @Test
    void givenCourse_whenAdded_thenStatus201andCreatedReturns() throws Exception {
        mockMvc.perform(post("/courses")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                "{\n" +
                                        "  \"title\": \"TEST\"\n" +
                                        "}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("TEST_CREATED")))
                .andDo(print());
    }

    @Test
    void givenCourse_whenUpdated_thenStatus202andUpdatedReturns() throws Exception {
        mockMvc.perform(put("/courses/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(
                                "{\n" +
                                        "  \"title\": \"TEST\"\n" +
                                        "}"))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("TEST_UPDATED")))
                .andDo(print());
    }

    @Test
    void givenCourse_whenDeleteCourse_thenStatus202andDeletedReturns() throws Exception {
        mockMvc.perform(delete("/courses/1"))
                .andExpect(status().isAccepted())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("TEST_DELETED")))
                .andDo(print());
    }

    @Test
    void givenCourse_whenDeleteNotExistedCourse_thenStatus404() throws Exception {
        mockMvc.perform(delete("/courses/100"))
                .andExpect(status().isNotFound());
    }
}