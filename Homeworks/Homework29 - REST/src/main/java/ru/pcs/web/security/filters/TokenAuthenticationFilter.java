package ru.pcs.web.security.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.pcs.web.dto.SignInForm;
import ru.pcs.web.models.Account;
import ru.pcs.web.repositories.AccountsRepository;
import ru.pcs.web.security.JwtTokenManager;
import ru.pcs.web.security.details.AccountUserDetails;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 16.11.2021
 * Homework33 - JWT
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Slf4j
public class TokenAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    public static final String TOKEN = "token";
    public static final String REFRESH_TOKEN = "refresh_token";

    private final ObjectMapper objectMapper;
    @Autowired
    private AccountsRepository accountsRepository;
    @Autowired
    private JwtTokenManager jwtTokenManager;

    public TokenAuthenticationFilter(AuthenticationManager authenticationManager,
                                     ObjectMapper objectMapper, AccountsRepository accountsRepository, JwtTokenManager jwtTokenManager) {
        super(authenticationManager);
        this.objectMapper = objectMapper;
        this.accountsRepository = accountsRepository;
        this.jwtTokenManager = jwtTokenManager;
    }



    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            SignInForm form = objectMapper.readValue(request.getReader(), SignInForm.class);
            log.info("Attempt authentication - email {}, password {}", form.getEmail(), form.getPassword());
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(form.getEmail(),
                    form.getPassword());

            return super.getAuthenticationManager().authenticate(token);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult) throws IOException {
        AccountUserDetails userDetails = (AccountUserDetails) authResult.getPrincipal();
        Account account = userDetails.getAccount();
        String token = jwtTokenManager.createAccountToken(account);
        String refreshToken = jwtTokenManager.createAccountRefreshToken(account);
        account.setToken(token);
        account.setRefreshToken(refreshToken);
        accountsRepository.save(account);

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        Map<String, String> result = new HashMap<>();
        result.put(TOKEN, token);
        result.put(REFRESH_TOKEN, refreshToken);

        objectMapper.writeValue(response.getWriter(), result);
    }
}
