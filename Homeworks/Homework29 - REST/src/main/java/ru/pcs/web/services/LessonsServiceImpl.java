package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.LessonDto;
import ru.pcs.web.models.Course;
import ru.pcs.web.models.Lesson;
import ru.pcs.web.repositories.CoursesRepository;
import ru.pcs.web.repositories.LessonsRepository;

import java.util.List;

/**
 * 07.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;
    private final CoursesRepository coursesRepository;

    @Override
    public List<LessonDto> getLessons(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Lesson> result = lessonsRepository.findAllByIsDeletedIsNull(request);
        return LessonDto.from(result.getContent());
    }

    @Override
    public LessonDto addLesson(LessonDto lesson) {
        Lesson newLesson = Lesson.builder()
                .id(lesson.getId())
                .name(lesson.getName())
                .build();

        lessonsRepository.save(newLesson);

        return LessonDto.from(newLesson);
    }

    @Override
    public LessonDto updateLesson(Long lessonId, LessonDto lesson) {
        Lesson currentLesson = lessonsRepository.getById(lessonId);
        currentLesson.setName(lesson.getName());
        lessonsRepository.save(currentLesson);
        return LessonDto.from(currentLesson);
    }

    @Override
    public void deleteLessonFromCourse(Long lessonId) {
        Lesson currentLesson = lessonsRepository.getById(lessonId);
        currentLesson.setCourse(null);
        lessonsRepository.save(currentLesson);
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson lesson = lessonsRepository.getById(lessonId);
        lesson.setIsDeleted(true);
        lessonsRepository.save(lesson);
    }

    @Override
    public LessonDto addLessonToCourse(Long lessonId, Long courseId) {
        Lesson existingLesson = lessonsRepository.getById(lessonId);
        Course course = coursesRepository.getById(courseId);
        existingLesson.setCourse(course);
        lessonsRepository.save(existingLesson);
        return LessonDto.from(existingLesson);
    }
}
