package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pcs.web.models.MethodInvocationCounter;
import ru.pcs.web.repositories.MethodInvocationCounterRepository;

/**
 * 18.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class MethodInvocationCounterServiceImpl implements MethodInvocationCounterService {

    @Autowired
    private final MethodInvocationCounterRepository invocationCounterRepository;

    @Override
    public MethodInvocationCounter getMethodInvocationCounter(String methodName) {
        return invocationCounterRepository.getMethodInvocationCounterByMethodName(methodName);
    }

    @Override
    public void addCount(MethodInvocationCounter methodInvocationCounter) {
        Long currentCount = methodInvocationCounter.getInvokeCount();
        currentCount++;
        methodInvocationCounter.setInvokeCount(currentCount);
        invocationCounterRepository.save(methodInvocationCounter);
    }

    @Override
    public void addMethodInvocationCounter(MethodInvocationCounter methodInvocationCounter) {
        invocationCounterRepository.save(methodInvocationCounter);
    }
}
