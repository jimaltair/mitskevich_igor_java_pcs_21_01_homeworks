package ru.pcs.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.pcs.web.models.Account;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 16.11.2021
 * Homework33 - JWT
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDto {
    private Long id;
    private Account.Role role;
    private Account.State state;
    private String email;
    private String password;
    private String token;
    private String refreshToken;

    public static AccountDto from(Account account){
        return AccountDto.builder()
                .id(account.getId())
                .role(account.getRole())
                .state(account.getState())
                .email(account.getEmail())
                .password(account.getPassword())
                .token(account.getToken())
                .refreshToken(account.getRefreshToken())
                .build();
    }

    public static List<AccountDto> from(List<Account> accounts) {
        return accounts.stream().map(AccountDto::from).collect(Collectors.toList());
    }
}
