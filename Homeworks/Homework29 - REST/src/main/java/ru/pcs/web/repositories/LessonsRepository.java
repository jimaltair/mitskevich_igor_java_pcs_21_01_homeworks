package ru.pcs.web.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Lesson;

/**
 * 07.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface LessonsRepository extends JpaRepository<Lesson, Long> {
    Page<Lesson> findAllByIsDeletedIsNull(Pageable pageable);
}
