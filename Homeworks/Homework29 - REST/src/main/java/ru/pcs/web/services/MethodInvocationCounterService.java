package ru.pcs.web.services;

import ru.pcs.web.models.MethodInvocationCounter;

/**
 * 18.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface MethodInvocationCounterService {
    MethodInvocationCounter getMethodInvocationCounter(String methodName);
    void addCount(MethodInvocationCounter methodInvocationCounter);
    void addMethodInvocationCounter(MethodInvocationCounter methodInvocationCounter);
}
