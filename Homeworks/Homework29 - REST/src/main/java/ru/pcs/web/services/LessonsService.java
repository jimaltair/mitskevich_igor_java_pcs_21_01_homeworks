package ru.pcs.web.services;

import ru.pcs.web.dto.LessonDto;

import java.util.List;

/**
 * 07.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface LessonsService {
    List<LessonDto> getLessons(int page, int size);

    LessonDto addLesson(LessonDto lesson);

    LessonDto updateLesson (Long lessonId, LessonDto lesson);

    void deleteLessonFromCourse(Long lessonId);

    void deleteLesson(Long lessonId);

    LessonDto addLessonToCourse(Long lessonId, Long courseId);
}
