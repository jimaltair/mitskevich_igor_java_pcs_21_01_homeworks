package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.StudentDto;
import ru.pcs.web.models.Course;
import ru.pcs.web.models.Student;
import ru.pcs.web.repositories.CoursesRepository;
import ru.pcs.web.repositories.StudentsRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 07.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;

    @Override
    public CourseDto getCourse(Long courseId) {
        return CourseDto.from(coursesRepository.findById(courseId).get());
    }

    @Override
    public List<CourseDto> getCourses(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Course> result = coursesRepository.findAllByIsDeletedIsNull(request);
        return CourseDto.from(new ArrayList<>(result.getContent()));
    }

    @Override
    public CourseDto addCourse(CourseDto course) {
        Course newCourse = Course.builder()
                .id(course.getId())
                .title(course.getTitle())
                .build();

        coursesRepository.save(newCourse);

        return CourseDto.from(newCourse);
    }

    @Override
    public CourseDto updateCourse(Long courseId, CourseDto course) {
        Course existedCourse = coursesRepository.getById(courseId);
        existedCourse.setTitle(course.getTitle());
        return CourseDto.from(coursesRepository.save(existedCourse));
    }

    @Override
    public CourseDto deleteCourse(Long courseId) {
        Course course = coursesRepository.getById(courseId);
        course.setIsDeleted(true);
        return CourseDto.from(coursesRepository.save(course));
    }
}
