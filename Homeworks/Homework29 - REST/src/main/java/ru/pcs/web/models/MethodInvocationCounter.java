package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 18.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class MethodInvocationCounter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "method_name")
    private String methodName;
    @Column(name = "invoke_count")
    private Long invokeCount;
}
