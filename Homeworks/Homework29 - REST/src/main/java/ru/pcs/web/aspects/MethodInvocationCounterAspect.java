package ru.pcs.web.aspects;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.MethodInvocationCounter;
import ru.pcs.web.services.MethodInvocationCounterService;

/**
 * 18.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Component
@Aspect
@RequiredArgsConstructor
@Slf4j
public class MethodInvocationCounterAspect {

    @Autowired
    private MethodInvocationCounterService invocationCounterService;

    @Around(value = "execution(* ru.pcs.web.controllers.*.*(..))")
    public ResponseEntity countMethodsInvokes(ProceedingJoinPoint joinPoint) throws Throwable {
        String methodName = joinPoint.getSignature().getName();
        MethodInvocationCounter invocationCounter = invocationCounterService.getMethodInvocationCounter(methodName);
        if (invocationCounter == null) {
            invocationCounter = new MethodInvocationCounter(null, methodName, 1L);
            invocationCounterService.addMethodInvocationCounter(invocationCounter);
        } else {
            invocationCounterService.addCount(invocationCounter);
        }
        ResponseEntity response = (ResponseEntity) joinPoint.proceed();
        return response;
    }
}
