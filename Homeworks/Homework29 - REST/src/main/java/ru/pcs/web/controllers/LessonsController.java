package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.LessonDto;
import ru.pcs.web.dto.LessonsResponse;
import ru.pcs.web.services.LessonsService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * 07.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Controller
@RequestMapping("/lessons")
@RequiredArgsConstructor
public class LessonsController {
    private final LessonsService lessonsService;

    /*
    получаем список всех уроков с пагинацией
     */
    @GetMapping
    public ResponseEntity<LessonsResponse> getLessons(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok().body(LessonsResponse.builder().data(lessonsService.getLessons(page, size)).build());
    }

    /*
    добавляем новый урок
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public LessonDto addLesson(@RequestBody LessonDto lesson) {
        return lessonsService.addLesson(lesson);
    }

    /*
    изменяем данные урока
     */
    @PutMapping(value = "/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public LessonDto updateLesson(@PathVariable("lesson-id") Long lessonId, @RequestBody LessonDto lesson) {
        return lessonsService.updateLesson(lessonId, lesson);
    }

    /*
    удаляем урок
     */
    @DeleteMapping(value = "/{lesson-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteLesson(@PathVariable("lesson-id") Long lessonId) {
        lessonsService.deleteLesson(lessonId);
    }

    /*
    добавляем существующий урок в курс
    */
    @PutMapping(value = "/add_lesson")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public LessonsResponse addLessonToCourse(@RequestParam("lesson-id") Long lessonId, @RequestParam("course-id") Long courseId) {
        return LessonsResponse.builder()
                .data(Collections.singletonList(lessonsService.addLessonToCourse(lessonId, courseId)))
                .build();
    }

    /*
    удаляем урок из курса
    */
    @PutMapping(value = "/del_lesson_from_course")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteLessonFromCourse(@RequestParam("lesson-id") Long lessonId) {
        lessonsService.deleteLessonFromCourse(lessonId);
    }
}
