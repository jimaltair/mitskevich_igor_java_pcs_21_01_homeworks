package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.CoursesResponse;
import ru.pcs.web.dto.StudentDto;
import ru.pcs.web.dto.StudentsResponse;
import ru.pcs.web.services.StudentsService;

import java.time.LocalDate;

/**
 * 07.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Controller
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentsController {
    private final StudentsService studentsService;

    /*
    получаем список всех студентов с пагинацией
    */
    @GetMapping
    public ResponseEntity<StudentsResponse> getStudents(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .headers(httpHeaders -> httpHeaders.add("dateTime", LocalDate.now().toString()))
                .body(StudentsResponse.builder().data(studentsService.getStudents(page, size)).build());
    }

    /*
    добавляем нового студента
    */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StudentDto addStudent(@RequestBody StudentDto student) {
        return studentsService.addStudent(student);
    }

    /*
    изменяем данные студента по id
    */
    @PutMapping(value = "/{student-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public StudentDto updateStudent(@PathVariable("student-id") Long studentId, @RequestBody StudentDto student) {
        return studentsService.updateStudent(studentId, student);
    }

    /*
    удаляем студента по id
    */
    @DeleteMapping(value = "/{student-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteStudent(@PathVariable("student-id") Long studentId) {
        studentsService.deleteStudent(studentId);
    }

    /*
    добавляем студенту с определённым id новый курс
    */
    @PostMapping(value = "/{student-id}/courses")
    @ResponseStatus(HttpStatus.CREATED)
    public CoursesResponse addCourseToStudent(@PathVariable("student-id") Long studentId,
                                              @RequestBody CourseDto course) {
        return CoursesResponse.builder()
                .data(studentsService.addCourseToStudent(studentId, course))
                .build();
    }
}
