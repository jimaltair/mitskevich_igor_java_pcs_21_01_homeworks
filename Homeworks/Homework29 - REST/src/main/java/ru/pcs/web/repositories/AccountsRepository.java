package ru.pcs.web.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Account;

import java.util.Optional;

/**
 * 16.11.2021
 * Homework33 - JWT
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface AccountsRepository extends JpaRepository<Account, Long> {
    Page<Account> findAllByIsDeletedIsNull(Pageable pageable);
    Optional<Account> findByEmail(String email);
    Optional<Account> findAccountByRefreshToken(String refreshToken);
}
