package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.models.Account;
import ru.pcs.web.repositories.AccountsRepository;
import ru.pcs.web.security.JwtTokenManager;

import java.util.List;


/**
 * 16.11.2021
 * Homework33 - JWT
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Service
@RequiredArgsConstructor
public class AccountsServiceImpl implements AccountsService {

    private final AccountsRepository accountsRepository;
    @Autowired
    private final PasswordEncoder passwordEncoder;

    @Autowired
    private final JwtTokenManager tokenManager;

    @Override
    public AccountDto addAccount(AccountDto account) {
        Account newAccount = Account.builder()
                .role(account.getRole())
                .state(account.getState())
                .email(account.getEmail())
                .password(passwordEncoder.encode(account.getPassword()))
                .build();

        accountsRepository.save(newAccount);

        return AccountDto.from(newAccount);
    }

    @Override
    public List<AccountDto> getAccounts(int page, int size) {
        PageRequest request = PageRequest.of(page, size, Sort.by("id"));
        Page<Account> result = accountsRepository.findAllByIsDeletedIsNull(request);
        return AccountDto.from(result.getContent());
    }

    @Override
    public AccountDto getAccountByRefreshToken(String refreshToken) {
        return AccountDto.from(accountsRepository.findAccountByRefreshToken(refreshToken).get());
    }

    @Override
    public void refreshAccountsTokens(AccountDto account) {
        Account currentAccount = accountsRepository.getById(account.getId());
        currentAccount.setToken(tokenManager.createAccountToken(currentAccount));
        currentAccount.setRefreshToken(tokenManager.createAccountRefreshToken(currentAccount));
        accountsRepository.save(currentAccount);
    }

}
