package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.MethodInvocationCounter;

/**
 * 18.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface MethodInvocationCounterRepository extends JpaRepository<MethodInvocationCounter, Long> {
    MethodInvocationCounter getMethodInvocationCounterByMethodName(String methodName);
}
