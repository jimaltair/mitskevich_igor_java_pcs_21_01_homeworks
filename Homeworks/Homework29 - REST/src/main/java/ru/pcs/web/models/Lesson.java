package ru.pcs.web.models;

import lombok.*;

import javax.persistence.*;

/**
 * 07.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "course")
@Entity
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
