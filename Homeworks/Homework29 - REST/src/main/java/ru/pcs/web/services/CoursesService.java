package ru.pcs.web.services;

import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.StudentDto;

import java.util.List;

/**
 * 07.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface CoursesService {
    CourseDto getCourse(Long courseId);

    List<CourseDto> getCourses(int page, int size);

    CourseDto addCourse(CourseDto course);

    CourseDto updateCourse(Long courseId, CourseDto course);

    CourseDto deleteCourse(Long courseId);
}
