package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.dto.CourseDto;
import ru.pcs.web.dto.CoursesResponse;
import ru.pcs.web.dto.StudentDto;
import ru.pcs.web.dto.StudentsResponse;
import ru.pcs.web.models.Course;
import ru.pcs.web.services.CoursesService;

import javax.persistence.EntityNotFoundException;

/**
 * 07.11.2021
 * Homework29 - REST
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Controller
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CoursesController {
    private final CoursesService coursesService;

    /*
    получаем список всех курсов с пагинацией
    */
    @GetMapping
    public ResponseEntity<CoursesResponse> getCourses(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok().body(CoursesResponse.builder().data(coursesService.getCourses(page, size)).build());
    }

    /*
    добавляем новый курс
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CourseDto> addCourse(@RequestBody CourseDto course) {
        return ResponseEntity.status(201).body(coursesService.addCourse(course));
    }

    /*
    изменяем данные курса по id
     */
    @PutMapping(value = "/{course-id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<CourseDto> updateCourse(@PathVariable("course-id") Long courseId, @RequestBody CourseDto course) {
        return ResponseEntity.accepted().body(coursesService.updateCourse(courseId, course));
    }

    /*
    удаляем курс по id
    */
    @DeleteMapping(value = "/{course-id}")
    public ResponseEntity<CourseDto> deleteCourse(@PathVariable("course-id") Long courseId) {
        CourseDto course = null;
        int status;
        try {
            course = coursesService.deleteCourse(courseId);
            status = 202;
        } catch (EntityNotFoundException e) {
            status = 404;
        }
        return ResponseEntity.status(status).body(course);
    }
}
