package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.dto.AccountsResponse;
import ru.pcs.web.services.AccountsService;

import java.util.HashMap;
import java.util.Map;

/**
 * 16.11.2021
 * Homework33 - JWT
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Controller
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountsController {

    private final AccountsService accountsService;

    @GetMapping
    public ResponseEntity<AccountsResponse> getAccounts(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .body(AccountsResponse.builder().data(accountsService.getAccounts(page, size)).build());
    }

    @PostMapping(value = "/refresh_token")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity refreshToken(@RequestBody AccountDto input) {
        String refreshToken = input.getRefreshToken();
        AccountDto account = accountsService.getAccountByRefreshToken(refreshToken);
        accountsService.refreshAccountsTokens(account);
        Map<String, String> result = new HashMap<>();
        result.put("token", account.getToken());
        result.put("refresh_token", account.getRefreshToken());
        return ResponseEntity.ok().body(result);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<AccountDto> addAccount(@RequestBody AccountDto account) {
        return ResponseEntity.status(201).body(accountsService.addAccount(account));
    }
}
