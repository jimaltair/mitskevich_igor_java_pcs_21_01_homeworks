package ru.pcs.web.services;

import ru.pcs.web.dto.AccountDto;

import java.util.List;

/**
 * 16.11.2021
 * Homework33 - JWT
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface AccountsService {
    AccountDto addAccount(AccountDto account);

    List<AccountDto> getAccounts(int page, int size);

    AccountDto getAccountByRefreshToken(String refreshToken);

    void refreshAccountsTokens(AccountDto account);
}
