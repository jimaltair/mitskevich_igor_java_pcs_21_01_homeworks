package ru.pcs.web.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Account;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

/**
 * 15.11.2021
 * Homework33 - JWT
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Component
@Slf4j
@Setter
public class JwtTokenManager {

    @Value("${jwt-token.secret-key:secret_key}")
    private String SECRET_KEY;

    public String createAccountToken(Account account) {
        Date expireDate = getExpireDate(14);
        return JWT.create()
                .withSubject(account.getId().toString())
                .withClaim("role", account.getRole().toString())
                .withClaim("state", account.getState().toString())
                .withClaim("email", account.getEmail())
                .withClaim("password", account.getPassword())
                .withExpiresAt(expireDate)
                .sign(Algorithm.HMAC256(SECRET_KEY));
    }

    public String createAccountRefreshToken(Account account) {
        Date expireDate = getExpireDate(365);
        return JWT.create()
                .withSubject(account.getId().toString())
                .withClaim("role", account.getRole().toString())
                .withClaim("state", account.getState().toString())
                .withClaim("email", account.getEmail())
                .withClaim("password", account.getPassword())
                .withExpiresAt(expireDate)
                .sign(Algorithm.HMAC256(SECRET_KEY));
    }

    private Date getExpireDate(int days){
        return Date.from(LocalDate.now().plusDays(days).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public boolean validateToken(String token) {
        try {
            DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256(SECRET_KEY)).build().verify(token);
            if (decodedJWT.getExpiresAt().before(new Date())) {
                return false;
            }
            return true;
        } catch (JWTVerificationException e) {
            log.error("JWT token is expired or invalid");
        }
        return false;
    }

    public Optional<Account> retrieveAccountFromToken(String token) {
        DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256(SECRET_KEY)).build().verify(token);
        Optional<Account> account = Optional.of(new Account());
        account.get().setId(Long.valueOf(decodedJWT.getClaim("sub").asString()));
        account.get().setRole(Account.Role.valueOf(decodedJWT.getClaim("role").asString()));
        account.get().setState(Account.State.valueOf(decodedJWT.getClaim("state").asString()));
        account.get().setEmail(decodedJWT.getClaim("email").asString());
        account.get().setPassword(decodedJWT.getClaim("password").asString());
        return account;
    }
}
