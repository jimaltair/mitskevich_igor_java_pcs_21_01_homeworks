package jaava.config;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * 06.10.2021
 * Homework16 - JAR
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Config {
    private String DB_USER;
    private String DB_PASSWORD;
    private String DB_URL;
    private String ORG_POSTGRESQL_DRIVER;
    private int DEFAULT_POOL_SIZE;

    public Config() {
        Properties properties = new Properties();
        try (FileReader reader = new FileReader("Homeworks/Homework16 - JAR/src/resources/hikari.properties")) {
            properties.load(reader);
            DB_USER = properties.getProperty("DB_USER");
            DB_PASSWORD = properties.getProperty("DB_PASSWORD");
            DB_URL = properties.getProperty("DB_URL");
            ORG_POSTGRESQL_DRIVER = properties.getProperty("ORG_POSTGRESQL_DRIVER");
            DEFAULT_POOL_SIZE = Integer.parseInt(properties.getProperty("DEFAULT_POOL_SIZE"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String getDB_USER() {
        return DB_USER;
    }

    public String getDB_PASSWORD() {
        return DB_PASSWORD;
    }

    public String getDB_URL() {
        return DB_URL;
    }

    public String getORG_POSTGRESQL_DRIVER() {
        return ORG_POSTGRESQL_DRIVER;
    }

    public int getDEFAULT_POOL_SIZE() {
        return DEFAULT_POOL_SIZE;
    }
}
