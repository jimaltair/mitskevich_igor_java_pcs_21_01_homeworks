package jaava.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import jaava.config.Config;

import javax.sql.DataSource;

/**
 * 06.10.2021
 * Homework16 - JAR
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class HikariDataSourceInitializer {
    private HikariConfig hikariConfig;
    private Config config;

    public HikariDataSourceInitializer() {
        config = new Config();
        hikariConfig = new HikariConfig();
        hikariConfig.setUsername(config.getDB_USER());
        hikariConfig.setPassword(config.getDB_PASSWORD());
        hikariConfig.setDriverClassName(config.getORG_POSTGRESQL_DRIVER());
        hikariConfig.setJdbcUrl(config.getDB_URL());
        hikariConfig.setMaximumPoolSize(config.getDEFAULT_POOL_SIZE());
    }

    public DataSource getHikariDataSource() {
        return new HikariDataSource(hikariConfig);
    }

    public void setMaximumPoolSize(int poolSize) {
        if (poolSize < 1) {
            hikariConfig.setMaximumPoolSize(config.getDEFAULT_POOL_SIZE());
        } else {
            hikariConfig.setMaximumPoolSize(poolSize);
        }
    }
}
