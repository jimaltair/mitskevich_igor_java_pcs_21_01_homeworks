package arraylist;

/**
 * 06.09.2021
 * Homework02 - ArrayList
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface List<E> {
    void add(E element);
    E get(int index);

    Iterator<E> iterator();
}
