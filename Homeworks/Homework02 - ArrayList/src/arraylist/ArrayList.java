package arraylist;

import java.util.Arrays;

/**
 * 06.09.2021
 * Homework02 - ArrayList
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class ArrayList<E> implements List<E> {

    private int size;
    private E[] elements;
    private static final int DEFAULT_SIZE = 10;
    private int count;

    public ArrayList() {
        size = DEFAULT_SIZE;
        elements = (E[]) new Object[size];
        count = 0;
    }

    public ArrayList(int size) {
        if (size > 0) {
            this.size = size;
            elements = (E[]) new Object[size];
            count = 0;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void add(E element) {
        if (count == elements.length) {
            increaseCapacity();
        }
        elements[count] = element;
        count++;
    }

    private void increaseCapacity() {
        int newLength = (int) (elements.length * 1.5 + 1);
        elements = Arrays.copyOf(elements, newLength);
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    private class ArrayListIterator implements Iterator<E> {

        int index = 0;
        E current = elements[index];

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public E next() {
            E value = current;
            current = elements[++index];
            return value;
        }
    }
}
