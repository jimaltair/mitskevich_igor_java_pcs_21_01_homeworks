package arraylist;

/**
 * 06.09.2021
 * Homework02 - ArrayList
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface Iterator<E> {
    boolean hasNext();
    E next();
}
