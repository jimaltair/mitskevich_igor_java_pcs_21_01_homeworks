package arraylist;

/**
 * 06.09.2021
 * Homework02 - ArrayList
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {
        List<String> strings = new ArrayList<>(5);

        strings.add("Hello!");
        strings.add("Marsel");
        strings.add("Bye");
        strings.add("Java");
        strings.add("C++");
        strings.add("Python");
        strings.add("Ruby");
        strings.add("Swift");
        strings.add("Rust");

        System.out.println(strings.get(0));
        System.out.println(strings.get(3));
        System.out.println(strings.get(5));
        System.out.println(strings.get(8));

        System.out.println("-----------------------------------------");

        Iterator<String> stringIterator = strings.iterator();

        while (stringIterator.hasNext()) {
            System.out.println(stringIterator.next());
        }

        System.out.println("-----------------------------------------");

        List<Integer> numbers = new ArrayList<>(5);

        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);
        numbers.add(6);
        numbers.add(7);
        numbers.add(8);
        numbers.add(9);

        System.out.println(numbers.get(0));
        System.out.println(numbers.get(3));
        System.out.println(numbers.get(5));
        System.out.println(numbers.get(8));

        System.out.println("-----------------------------------------");

        Iterator<Integer> numberIterator = numbers.iterator();

        while (numberIterator.hasNext()) {
            System.out.println(numberIterator.next());
        }
    }
}
