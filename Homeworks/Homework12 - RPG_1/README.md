```
Максимально творческое задание :)

Использовать изученые паттерны для моделирования классов RPG-игры.

1. Оружие - есть разные типы оружий, каждое оружие имеет свой набор характеристик, также есть общие характеристики для всех оружий (например, урон). Применить паттерны "Builder" и "Prototype".

2. Оружейная - есть разные типы оружейных, которые делают оружие. Например, оружейная для металлических мечей и т.д., оружейная для деревянных изделий, каменных и т.д. Применить паттерн "Абстрактная фабрика".
```