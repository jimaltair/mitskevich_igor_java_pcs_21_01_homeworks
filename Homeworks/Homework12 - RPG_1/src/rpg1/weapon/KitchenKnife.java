package rpg1.weapon;

import java.util.StringJoiner;

/**
 * 27.09.2021
 * Homework12 - RPG_1
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class KitchenKnife extends Weapon {

    public KitchenKnife() {
        super("Kitchen knife", 2, 10, 0.8);
    }

    private KitchenKnife(KitchenKnifeBuilder builder) {
        this.name = builder.name;
        this.damage = builder.damage;
        this.hitPoints = builder.hitPoints;
        this.weight = builder.weight;
    }

    private static class KitchenKnifeBuilder {
        private String name;
        private int damage;
        private int hitPoints;
        private double weight;

        public KitchenKnifeBuilder buildName(String name) {
            this.name = name;
            return this;
        }

        public KitchenKnifeBuilder buildDamage(int damage) {
            this.damage = damage;
            return this;
        }

        public KitchenKnifeBuilder buildHitPoints(int hitPoints) {
            this.hitPoints = hitPoints;
            return this;
        }

        public KitchenKnifeBuilder buildWeight(double weight) {
            this.weight = weight;
            return this;
        }

        public KitchenKnife build() {
            return new KitchenKnifeBuilder()
                    .buildName(this.name)
                    .buildDamage(this.damage)
                    .buildHitPoints(this.hitPoints)
                    .buildWeight(this.weight)
                    .build();
        }
    }

    @Override
    public Weapon copy() {
        return new KitchenKnife.KitchenKnifeBuilder()
                .buildName(this.name)
                .buildDamage(this.damage)
                .buildHitPoints(this.hitPoints)
                .buildWeight(this.weight)
                .build();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", KitchenKnife.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("damage=" + damage)
                .add("hitPoints=" + hitPoints)
                .add("weight=" + weight)
                .toString();
    }
}
