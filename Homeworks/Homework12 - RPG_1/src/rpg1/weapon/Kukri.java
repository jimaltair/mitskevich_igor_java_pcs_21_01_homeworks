package rpg1.weapon;

import java.util.List;
import java.util.StringJoiner;

/**
 * 27.09.2021
 * Homework12 - RPG_1
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Kukri extends Weapon {

    List<SpecialSkill> skills;
    private int cuttingSpeed;

    public Kukri(int cuttingSpeed, List<SpecialSkill> skills) {
        super("Kukri", 5, 50, 2.8);
        this.cuttingSpeed = cuttingSpeed;
    }

    private Kukri(KukriBuilder builder) {
        this.name = builder.name;
        this.damage = builder.damage;
        this.hitPoints = builder.hitPoints;
        this.weight = builder.weight;
        this.cuttingSpeed = builder.cuttingSpeed;
        this.skills = builder.skills;
    }

    public static class KukriBuilder {
        private String name;
        private int damage;
        private int hitPoints;
        private double weight;
        private int cuttingSpeed;
        private List<SpecialSkill> skills;

        public KukriBuilder buildName(String name) {
            this.name = name;
            return this;
        }

        public KukriBuilder buildDamage(int damage) {
            this.damage = damage;
            return this;
        }

        public KukriBuilder buildHitPoints(int hitPoints) {
            this.hitPoints = hitPoints;
            return this;
        }

        public KukriBuilder buildWeight(double weight) {
            this.weight = weight;
            return this;
        }

        public KukriBuilder buildCuttingSpeed(int cuttingSpeed) {
            this.cuttingSpeed = cuttingSpeed;
            return this;
        }

        public KukriBuilder buildSkills(List<SpecialSkill> skills) {
            this.skills = skills;
            return this;
        }

        public Kukri build() {
            return new Kukri(this);
        }
    }

    @Override
    public Weapon copy() {
        return new KukriBuilder()
                .buildName(this.name)
                .buildDamage(this.damage)
                .buildHitPoints(this.hitPoints)
                .buildWeight(this.weight)
                .buildCuttingSpeed(this.cuttingSpeed)
                .buildSkills(this.skills).build();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Kukri.class.getSimpleName() + "[", "]")
                .add("skills=" + skills)
                .add("cuttingSpeed=" + cuttingSpeed)
                .add("name='" + name + "'")
                .add("damage=" + damage)
                .add("hitPoints=" + hitPoints)
                .add("weight=" + weight)
                .toString();
    }
}
