package rpg1.weapon;

/**
 * 27.09.2021
 * Homework12 - RPG_1
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public abstract class Weapon {

    String name;
    int damage;
    int hitPoints;
    double weight;

    protected Weapon() {
    }

    public Weapon(String name, int damage, int hitPoints, double weight) {
        this.name = name;
        this.damage = damage;
        this.hitPoints = hitPoints;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public abstract Weapon copy();
}
