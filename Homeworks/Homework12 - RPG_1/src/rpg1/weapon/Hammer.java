package rpg1.weapon;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

/**
 * 27.09.2021
 * Homework12 - RPG_1
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Hammer extends Weapon {

    private List<SpecialSkill> skills;
    private int impactForce;

    public Hammer(int impactForce, List<SpecialSkill> skills) {
        super("Hammer", 30, 500, 20);
        this.impactForce = impactForce;
    }

    private Hammer(HammerBuilder builder) {
        this.name = builder.name;
        this.damage = builder.damage;
        this.hitPoints = builder.hitPoints;
        this.weight = builder.weight;
        this.impactForce = builder.impactForce;
        this.skills = builder.skills;
    }

    public static class HammerBuilder {
        private String name;
        private int damage;
        private int hitPoints;
        private double weight;
        private int impactForce;
        private List<SpecialSkill> skills;

        public HammerBuilder buildName(String name) {
            this.name = name;
            return this;
        }

        public HammerBuilder buildDamage(int damage) {
            this.damage = damage;
            return this;
        }

        public HammerBuilder buildHitPoints(int hitPoints) {
            this.hitPoints = hitPoints;
            return this;
        }

        public HammerBuilder buildWeight(double weight) {
            this.weight = weight;
            return this;
        }

        public HammerBuilder buildImpactForce(int impactForce) {
            this.impactForce = impactForce;
            return this;
        }

        public HammerBuilder buildSkills(List<SpecialSkill> skills) {
            this.skills = skills;
            return this;
        }

        public Hammer build() {
            return new Hammer(this);
        }
    }

    public static HammerBuilder hammerBuilder() {
        return new HammerBuilder();
    }

    public void setSkills(SpecialSkill...skills) {
        this.skills = Arrays.asList(skills);
    }

    public void setImpactForce(int impactForce) {
        this.impactForce = impactForce;
    }

    @Override
    public Weapon copy() {
        return new HammerBuilder()
                .buildName(this.name)
                .buildDamage(this.damage)
                .buildHitPoints(this.hitPoints)
                .buildWeight(this.weight)
                .buildImpactForce(this.impactForce)
                .buildSkills(this.skills).build();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Hammer.class.getSimpleName() + "[", "]")
                .add("skills=" + skills)
                .add("impactForce=" + impactForce)
                .add("name='" + name + "'")
                .add("damage=" + damage)
                .add("hitPoints=" + hitPoints)
                .add("weight=" + weight)
                .toString();
    }
}
