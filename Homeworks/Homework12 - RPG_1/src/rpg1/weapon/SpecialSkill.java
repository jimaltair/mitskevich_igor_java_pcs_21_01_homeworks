package rpg1.weapon;

/**
 * 27.09.2021
 * Homework12 - RPG_1
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public enum SpecialSkill {
    THUNDER_STRIKE,
    THUNDER_JUMP,
    SILENT_KILL,
    RAPID_CUTTING,
    STUNNING_WEAK_ENEMY
}
