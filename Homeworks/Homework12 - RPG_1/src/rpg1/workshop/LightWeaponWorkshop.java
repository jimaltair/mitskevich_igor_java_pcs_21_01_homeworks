package rpg1.workshop;

import rpg1.weapon.KitchenKnife;
import rpg1.weapon.Kukri;
import rpg1.weapon.SpecialSkill;
import rpg1.weapon.Weapon;

import java.util.Arrays;
import java.util.UUID;

/**
 * 27.09.2021
 * Homework12 - RPG_1
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class LightWeaponWorkshop implements WeaponWorkshop {

    @Override
    public Weapon createWeapon(String weaponType) {
        Weapon lightWeapon = null;
        if (weaponType.equalsIgnoreCase("kitchen knife")) {
            KitchenKnife kitchenKnife = new KitchenKnife();
            lightWeapon = kitchenKnife;
        } else if (weaponType.equalsIgnoreCase("kukri")) {
            Kukri kukri = new Kukri(100, Arrays.asList(SpecialSkill.RAPID_CUTTING));
            lightWeapon = kukri;
        } else if (weaponType.equalsIgnoreCase("improved kukri")) {
            Weapon improvedKukri = new Kukri(100, Arrays.asList(SpecialSkill.RAPID_CUTTING,
                    SpecialSkill.SILENT_KILL));
            lightWeapon = improvedKukri;
        }
        engraveSerialNumber(lightWeapon);
        return lightWeapon;
    }

    @Override
    public void repair(Weapon weapon) {
        System.out.println(weapon.getName() + " передан в ближайший эльфийский сервисный центр");
    }

    private void engraveSerialNumber(Weapon weapon) {
        if (weapon != null) {
            System.out.println(weapon.getName() + " произведен в Санкт-Петербургской мастерской прецизионного " +
                    "эльфийского машиностроения. Серийный номер: "
                    + UUID.nameUUIDFromBytes(weapon.getName().getBytes()));
        }
    }
}
