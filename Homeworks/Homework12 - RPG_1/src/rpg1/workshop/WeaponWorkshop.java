package rpg1.workshop;

import rpg1.weapon.Weapon;

/**
 * 27.09.2021
 * Homework12 - RPG_1
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface WeaponWorkshop {
    Weapon createWeapon(String weaponType);
    void repair(Weapon weapon);
}
