package rpg1.workshop;

/**
 * 27.09.2021
 * Homework12 - RPG_1
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class WorkshopProvider {

    public static WeaponWorkshop getWorkshop(WorkshopType workshopType) {
        if (workshopType == WorkshopType.LIGHT) {
            return new LightWeaponWorkshop();
        } else if (workshopType == WorkshopType.HEAVY) {
            return new HeavyWeaponWorkshop();
        }
        return null;
    }

    public enum WorkshopType {
        LIGHT, HEAVY
    }
}
