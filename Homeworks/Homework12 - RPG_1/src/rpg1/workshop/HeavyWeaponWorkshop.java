package rpg1.workshop;

import rpg1.weapon.Hammer;
import rpg1.weapon.SpecialSkill;
import rpg1.weapon.Weapon;

import java.util.Arrays;
import java.util.UUID;

/**
 * 27.09.2021
 * Homework12 - RPG_1
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class HeavyWeaponWorkshop implements WeaponWorkshop {

    @Override
    public Weapon createWeapon(String weaponType) {
        Weapon heavyWeapon = null;
        if (weaponType.equalsIgnoreCase("hammer")) {
            heavyWeapon = new Hammer(1000, Arrays.asList(SpecialSkill.THUNDER_STRIKE));
        } else if (weaponType.equalsIgnoreCase("improved hammer")) {
            heavyWeapon = new Hammer(1000, Arrays.asList(SpecialSkill.THUNDER_STRIKE,
                    SpecialSkill.THUNDER_JUMP));
        }
        engraveSerialNumber(heavyWeapon);
        return heavyWeapon;
    }

    @Override
    public void repair(Weapon weapon) {
        System.out.println(weapon.getName() + " передан в ближайший гномий сервисный центр");
    }

    private void engraveSerialNumber(Weapon weapon) {
        if (weapon != null) {
            System.out.println(weapon.getName() + " произведен в Уральской мастерской тяжёлого гномьево " +
                    "машиностроения. Серийный номер: " +
                    UUID.nameUUIDFromBytes(weapon.getName().getBytes()));
        }
    }
}
