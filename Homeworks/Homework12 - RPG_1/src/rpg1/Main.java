package rpg1;

import rpg1.weapon.Hammer;
import rpg1.weapon.Kukri;
import rpg1.weapon.SpecialSkill;
import rpg1.weapon.Weapon;
import rpg1.workshop.WeaponWorkshop;
import rpg1.workshop.WorkshopProvider;

import java.util.Arrays;

/**
 * 27.09.2021
 * Homework12 - RPG_1
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {

        // Builder
        Hammer hammer = new Hammer.HammerBuilder()
                .buildName("Mega Hammer")
                .buildDamage(100)
                .buildHitPoints(1000)
                .buildImpactForce(3500)
                .buildWeight(35)
                .build();
        System.out.println(hammer);

        Kukri improvedKukri = new Kukri.KukriBuilder()
                .buildName("Improved Kukri")
                .buildDamage(7)
                .buildHitPoints(75)
                .buildWeight(2.5)
                .buildSkills(Arrays.asList(SpecialSkill.RAPID_CUTTING,
                        SpecialSkill.SILENT_KILL, SpecialSkill.STUNNING_WEAK_ENEMY)).build();
        System.out.println(improvedKukri);

        System.out.println("----------------------------------------------");

        // Prototype
        Hammer basicHammer = new Hammer(1000, Arrays.asList(SpecialSkill.THUNDER_STRIKE));
        Hammer improveBasicHammer = (Hammer) basicHammer.copy();
        improveBasicHammer.setSkills(SpecialSkill.THUNDER_JUMP);
        System.out.println(basicHammer);
        System.out.println(improveBasicHammer);

        System.out.println("----------------------------------------------");

        //Abstract Factory
        WeaponWorkshop lightWeaponWorkshop = WorkshopProvider.getWorkshop(WorkshopProvider.WorkshopType.LIGHT);
        WeaponWorkshop heavyWeaponWorkshop = WorkshopProvider.getWorkshop(WorkshopProvider.WorkshopType.HEAVY);

        Weapon workshopHammer = heavyWeaponWorkshop.createWeapon("hammer");
        Weapon workshopKukri = lightWeaponWorkshop.createWeapon("kukri");

        heavyWeaponWorkshop.repair(workshopHammer);
        lightWeaponWorkshop.repair(workshopKukri);
    }
}
