package config;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

/**
 * 08.10.2021
 * Homework17 - Maven
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Config {

    private static final Logger logger = Logger.getLogger(Config.class);

    private String DB_USER;
    private String DB_PASSWORD;
    private String DB_URL;
    private String ORG_POSTGRESQL_DRIVER;
    private int DEFAULT_POOL_SIZE;

    public Config() {
        Properties properties = new Properties();
        try {
            properties.load(getClass().getResourceAsStream("/hikari.properties"));
            DB_USER = properties.getProperty("DB_USER");
            DB_PASSWORD = properties.getProperty("DB_PASSWORD");
            DB_URL = properties.getProperty("DB_URL");
            ORG_POSTGRESQL_DRIVER = properties.getProperty("ORG_POSTGRESQL_DRIVER");
            DEFAULT_POOL_SIZE = Integer.parseInt(properties.getProperty("DEFAULT_POOL_SIZE"));
        } catch (IOException e) {
            logger.error(e);
            throw new IllegalArgumentException(e);
        }
    }

    public String getDB_USER() {
        return DB_USER;
    }

    public String getDB_PASSWORD() {
        return DB_PASSWORD;
    }

    public String getDB_URL() {
        return DB_URL;
    }

    public String getORG_POSTGRESQL_DRIVER() {
        return ORG_POSTGRESQL_DRIVER;
    }

    public int getDEFAULT_POOL_SIZE() {
        return DEFAULT_POOL_SIZE;
    }
}
