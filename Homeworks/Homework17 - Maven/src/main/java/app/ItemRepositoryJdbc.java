package app;

import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * 08.10.2021
 * Homework17 - Maven
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class ItemRepositoryJdbc {

    private final static Logger logger = Logger.getLogger(ItemRepositoryJdbc.class);

    //language=SQL
    private static final String SQL_SELECT_ALL = "select item_id, name_item, price, amount from item order by item_id limit ? offset ?;";

    private final DataSource dataSource;

    private static final Function<ResultSet, Item> itemMapper = resultSet -> {
        try {
            Long id = resultSet.getLong(1);
            String name = resultSet.getString(2);
            Double price = resultSet.getDouble(3);
            Integer amount = resultSet.getInt(4);
            return new Item(id, name, price, amount);
        } catch (SQLException e) {
            logger.error(e);
            throw new IllegalArgumentException(e);
        }
    };

    public ItemRepositoryJdbc(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Item> findAll(int page, int size) {
        logger.info("find items on page: " + page + ", size: " + size);
        List<Item> items = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {
            logger.info("create connection");
            statement.setInt(1, size);
            statement.setInt(2, page * size);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    items.add(itemMapper.apply(resultSet));
                }
                logger.info("finish query");
                return items;
            }
        } catch (SQLException e) {
            logger.error(e);
            throw new IllegalArgumentException(e);
        }

    }
}
