package app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import config.Config;
import org.apache.log4j.Logger;

import javax.sql.DataSource;

/**
 * 08.10.2021
 * Homework17 - Maven
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class HikariDataSourceInitializer {

    private final static Logger logger = Logger.getLogger(HikariDataSourceInitializer.class);
    private HikariConfig hikariConfig;
    private Config config;

    public HikariDataSourceInitializer() {
        config = new Config();
        hikariConfig = new HikariConfig();
        hikariConfig.setUsername(config.getDB_USER());
        hikariConfig.setPassword(config.getDB_PASSWORD());
        hikariConfig.setDriverClassName(config.getORG_POSTGRESQL_DRIVER());
        hikariConfig.setJdbcUrl(config.getDB_URL());
        hikariConfig.setMaximumPoolSize(config.getDEFAULT_POOL_SIZE());
        logger.info("load Hikari configs");
    }

    public DataSource getHikariDataSource() {
        return new HikariDataSource(hikariConfig);
    }

    public void setMaximumPoolSize(int poolSize) {
        if (poolSize < 1) {
            hikariConfig.setMaximumPoolSize(config.getDEFAULT_POOL_SIZE());
        } else {
            logger.warn("hikari pool size was changed from " + hikariConfig.getMaximumPoolSize() + " to " + poolSize);
            hikariConfig.setMaximumPoolSize(poolSize);
        }
    }
}
