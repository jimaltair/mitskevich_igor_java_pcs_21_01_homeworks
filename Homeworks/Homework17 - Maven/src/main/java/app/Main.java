package app;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 08.10.2021
 * Homework17 - Maven
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@Parameters(separators = "=")
public class Main {

    private static final Logger logger = Logger.getLogger(Main.class);

    @Parameter(names = {"--hikari-pool-size"})
    private int poolSize;

    public static void main(String[] args) {

        HikariDataSourceInitializer hikariDSInitializer = new HikariDataSourceInitializer();
        DataSource hikariDataSource = hikariDSInitializer.getHikariDataSource();
        ItemRepositoryJdbc itemRepository = new ItemRepositoryJdbc(hikariDataSource);

        Main main = new Main();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        hikariDSInitializer.setMaximumPoolSize(main.poolSize);

        if (args.length > 0) {
            logger.warn("Incoming arguments in Main.class: " + Arrays.toString(args));
        }

        System.out.println("Do you want to load item`s list from database? yes/no");
        Scanner scanner = new Scanner(System.in);
        if (!scanner.next().equalsIgnoreCase("yes")) {
            System.exit(0);
        } else {
            // имитируются множественные select`ы из БД
            ExecutorService service = Executors.newFixedThreadPool(100);

            for (int i = 0; i < 100; i++) {
                service.submit(() -> {
                    for (int j = 0; j < 10; j++) {
                        try {
                            Thread.sleep(100);
                            System.out.println(itemRepository.findAll(0, 10));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            service.shutdown();
        }
    }
}
