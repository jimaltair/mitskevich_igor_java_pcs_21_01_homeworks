insert into item(name_item, price, amount)
values ('Item1', 10, 25),
       ('Item2', 20, 50),
       ('Item3', 30, 75),
       ('Item4', 40, 100),
       ('Item5', 50, 200),
       ('Item6', 60, 1),
       ('Item7', 70, 3),
       ('Item8', 80, 5),
       ('Item9', 90, 7),
       ('Item10', 100, 9);