DROP TABLE if exists item;

CREATE TABLE item
(
    item_id  BIGINT AUTO_INCREMENT NOT NULL,
    name_item VARCHAR(50),
    price     DECIMAL(8, 2),
    amount    INTEGER
);