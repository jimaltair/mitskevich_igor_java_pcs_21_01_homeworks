package app;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

/**
 * 13.10.2021
 * Homework17 - Maven
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
class ItemRepositoryJdbcTest {

    private ItemRepositoryJdbc itemRepositoryJdbc;
    @Spy
    private DataSource dataSource;

    @BeforeEach
    public void setUp() {
        dataSource = Mockito.spy(new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("schema.sql")
                .addScript("data.sql")
                .build());

        this.itemRepositoryJdbc = new ItemRepositoryJdbc(dataSource);
    }

    @Test
    public void selectTest1() {
        List<Item> items = itemRepositoryJdbc.findAll(0, 10);
        assertNotNull(items);
        assertTrue(items.size() != 0);
    }

    @Test
    public void selectTest2() throws SQLException {
        when(dataSource.getConnection()).thenThrow(new SQLException());
        assertThrows(IllegalArgumentException.class, () -> itemRepositoryJdbc.findAll(0, 10));
    }
}