package fileanalisys.chain;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public abstract class AbstractCarHandler implements CarHandler {

    protected CarHandler next;
    private static final Path OUTPUT_DIRECTORY = Paths.get("Homeworks/Homework13 - FileAnalysis/resources/output_chain");

    @Override
    public CarHandler setNextHandler(CarHandler handler) {
        this.next = handler;
        return handler;
    }

    protected void nextHandler(List<String> lines) {
        if (next != null) {
            next.handle(lines);
        }
    }

    protected void writeToOutputFile(Path outputFile, List<String> data) {
        try {
            if (!Files.exists(OUTPUT_DIRECTORY)) {
                Files.createDirectory(OUTPUT_DIRECTORY);
            }
            Files.write(outputFile, data);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
