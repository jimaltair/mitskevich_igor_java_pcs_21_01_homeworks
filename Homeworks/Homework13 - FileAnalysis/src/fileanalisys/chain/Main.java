package fileanalisys.chain;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * 29.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {
        /**
         *  Цепочка ответственности
         */
        Path fileName = Paths.get("Homeworks/Homework13 - FileAnalysis/resources/input.txt");

        List<String> carDataList;
        try {
            carDataList = Files.readAllLines(fileName);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        LicensePlateCarHandler plateCarHandler = new LicensePlateCarHandler();

        plateCarHandler.setNextHandler(new ModelCarHandler())
                .setNextHandler(new ColorCarHandler())
                .setNextHandler(new MileageCarHandler())
                .setNextHandler(new PriceCarHandler());

        plateCarHandler.handle(carDataList);
    }
}
