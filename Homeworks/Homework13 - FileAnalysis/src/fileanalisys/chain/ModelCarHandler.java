package fileanalisys.chain;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class ModelCarHandler extends AbstractCarHandler {

    private final Path outputFile = Paths.get("Homeworks/Homework13 - FileAnalysis/resources/output_chain/models.txt");

    @Override
    public void handle(List<String> lines) {
        List<String> licensePlates = lines.stream()
                .map(stringToModelFunction())
                .collect(Collectors.toList());
        writeToOutputFile(outputFile, licensePlates);

        nextHandler(lines);
    }

    private Function<String, String> stringToModelFunction() {
        return line -> line.split("]\\[")[1];
    }
}
