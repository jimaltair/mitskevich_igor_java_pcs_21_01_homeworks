package fileanalisys.chain;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class ColorCarHandler extends AbstractCarHandler {

    private final Path outputFile = Paths.get("Homeworks/Homework13 - FileAnalysis/resources/output_chain/colors.txt");

    @Override
    public void handle(List<String> lines) {
        List<String> licensePlates = lines.stream()
                .map(stringToColorFunction())
                .collect(Collectors.toList());
        writeToOutputFile(outputFile, licensePlates);

        nextHandler(lines);
    }

    private Function<String, String> stringToColorFunction() {
        return line -> line.split("]\\[")[2];
    }
}
