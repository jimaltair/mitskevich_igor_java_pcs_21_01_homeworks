package fileanalisys.chain;

import java.util.List;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface CarHandler {

    void handle(List<String> lines);
    CarHandler setNextHandler(CarHandler handler);
}
