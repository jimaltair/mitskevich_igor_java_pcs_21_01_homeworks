package fileanalisys.chain;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class PriceCarHandler extends AbstractCarHandler {

    private final Path outputFile = Paths.get("Homeworks/Homework13 - FileAnalysis/resources/output_chain/prices.txt");

    @Override
    public void handle(List<String> lines) {
        List<String> list = lines.stream()
                .map(stringToPriceFunction())
                .collect(Collectors.toList());
        writeToOutputFile(outputFile, list);

        nextHandler(lines);
    }

    private Function<String, String> stringToPriceFunction() {
        return line -> {
            String s = line.split("]\\[")[4];
            return s.substring(0, s.length() - 1);
        };
    }
}
