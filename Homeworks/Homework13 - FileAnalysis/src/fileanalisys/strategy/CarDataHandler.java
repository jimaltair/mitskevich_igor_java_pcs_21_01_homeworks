package fileanalisys.strategy;

import java.nio.file.Path;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class CarDataHandler {

    private Path path;
    private DataReader dataReader;

    public CarDataHandler(Path path) {
        this.path = path;
    }

    public void setDataReader(DataReader dataReader) {
        this.dataReader = dataReader;
    }

    public void readData() {
        dataReader.readData(path);
    }

    public void printData() {
        dataReader.getData().forEach(System.out::println);
    }
}
