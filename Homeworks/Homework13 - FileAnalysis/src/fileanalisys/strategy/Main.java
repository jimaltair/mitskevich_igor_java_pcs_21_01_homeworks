package fileanalisys.strategy;


import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {
        /**
         * Стратегия
         */
        Path fileName = Paths.get("Homeworks/Homework13 - FileAnalysis/resources/input.txt");

        CarDataHandler carDataHandler = new CarDataHandler(fileName);
        StreamAPIDataReader streamAPIDataReader = new StreamAPIDataReader();
        BufferedReaderDataReader buffDataReader = new BufferedReaderDataReader();

        // чтение файла с помощью StreamAPI
        carDataHandler.setDataReader(streamAPIDataReader);
        carDataHandler.readData();
        carDataHandler.printData();

        System.out.println("----------------------------------------------");

        // чтение файла с помощью обычного построчного считывания через BufferedReader
        carDataHandler.setDataReader(buffDataReader);
        carDataHandler.readData();
        carDataHandler.printData();
    }
}
