package fileanalisys.strategy;

import java.nio.file.Path;
import java.util.List;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface DataReader {

    void readData(Path path);
    List<String> getData();
}
