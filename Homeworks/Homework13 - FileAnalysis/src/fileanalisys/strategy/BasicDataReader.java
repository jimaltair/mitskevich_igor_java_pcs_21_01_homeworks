package fileanalisys.strategy;

import java.util.ArrayList;
import java.util.List;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public abstract class BasicDataReader implements DataReader {

    protected List<String> data;

    protected BasicDataReader() {
        data = new ArrayList<>();
    }

    public List<String> getData() {
        return data;
    }
}