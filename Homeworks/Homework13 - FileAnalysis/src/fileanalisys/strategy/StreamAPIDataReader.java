package fileanalisys.strategy;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class StreamAPIDataReader extends BasicDataReader {

    @Override
    public void readData(Path path) {
        try {
            data = Files.readAllLines(path);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}