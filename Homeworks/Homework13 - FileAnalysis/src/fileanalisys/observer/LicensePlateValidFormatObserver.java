package fileanalisys.observer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class LicensePlateValidFormatObserver implements Observer {

    private static final Path BLACK_LIST =
            Paths.get("Homeworks/Homework13 - FileAnalysis/resources/output_observer/black_list.txt");
    private List<String> blackList;

    public LicensePlateValidFormatObserver() {
        blackList = new ArrayList<>();
    }

    @Override
    public void update(List<String> list) {
        list.stream().
                filter(licensePlate -> !isFormatValid(licensePlate))
                .peek(this::printLicensePlateWithInvalidFormat)
                .forEach(this::addLicensePlateToBlackList);
        writeBlackListToFile();
    }

    private static boolean isFormatValid(String licensePlate) {
        return licensePlate.matches("[А-Я][0-9][0-9][0-9][А-Я][А-Я]");

    }

    private void printLicensePlateWithInvalidFormat(String licensePlate) {
        System.out.println("Госномер " + licensePlate + " внесён в чёрный список");
    }

    private void addLicensePlateToBlackList(String licensePlate){
        blackList.add(licensePlate);
    }

    private void writeBlackListToFile() {
        try {
            Files.write(BLACK_LIST, blackList, StandardOpenOption.CREATE);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
