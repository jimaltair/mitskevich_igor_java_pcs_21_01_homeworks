package fileanalisys.observer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class LicensePlatesProcessor implements Observable {

    private static final Path OUTPUT_FILE =
            Paths.get("Homeworks/Homework13 - FileAnalysis/resources/output_observer/license_plates.txt");
    private static final Path OUTPUT_DIRECTORY =
            Paths.get("Homeworks/Homework13 - FileAnalysis/resources/output_observer");
    private final Path inputFile;
    private List<Observer> observers;


    public LicensePlatesProcessor(Path inputFile) {
        this.inputFile = inputFile;
        observers = new LinkedList<>();
    }

    public void handle() {
        List<String> carDataList = readCarData();
        List<String> licensePlates = getLicensePlatesList(carDataList);
        writeToOutputFile(licensePlates);
        notifyObservers(licensePlates);
    }

    private List<String> readCarData() {
        List<String> carDataList;
        try {
            carDataList = Files.readAllLines(inputFile);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return carDataList;
    }

    private List<String> getLicensePlatesList(List<String> carDataList) {
        return carDataList.stream()
                .map(stringToLicensePlateFunction())
                .collect(Collectors.toList());
    }

    private void writeToOutputFile(List<String> data) {
        try {
            if (!Files.exists(OUTPUT_DIRECTORY)) {
                Files.createDirectory(OUTPUT_DIRECTORY);
            }
            Files.write(OUTPUT_FILE, data);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private Function<String, String> stringToLicensePlateFunction() {
        return line -> line.split("]\\[")[0].substring(1);
    }

    @Override
    public void registerObserver(Observer...observer) {
        observers.addAll(Arrays.asList(observer));
    }

    @Override
    public void removeObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(List<String> list) {
        observers.forEach(observer -> observer.update(list));
    }
}
