package fileanalisys.observer;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {
        /**
         *  Наблюдатель
         */
        Path fileName = Paths.get("Homeworks/Homework13 - FileAnalysis/resources/input.txt");

        LicensePlatesProcessor licensePlatesProcessor = new LicensePlatesProcessor(fileName);
        StolenCarsObserver stolenCarsObserver = new StolenCarsObserver();
        LicensePlateValidFormatObserver formatObserver = new LicensePlateValidFormatObserver();

        licensePlatesProcessor.registerObserver(stolenCarsObserver, formatObserver);

        licensePlatesProcessor.handle();
    }
}
