package fileanalisys.observer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class StolenCarsObserver implements Observer {

    private static final Path STOLEN_CARS = Paths.get("Homeworks/Homework13 - FileAnalysis/resources/stolen_cars.txt");

    @Override
    public void update(List<String> list) {
        list.stream().
                filter(getListOfStolenCars()::contains)
                .forEach(StolenCarsObserver::actionWithLicensePlate);
    }

    private List<String> getListOfStolenCars() {
        try {
            return Files.readAllLines(STOLEN_CARS);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private static void actionWithLicensePlate(String licensePlate) {
        System.out.println("Госномер " + licensePlate + " находится в списке угнанных автомобилей");
    }
}
