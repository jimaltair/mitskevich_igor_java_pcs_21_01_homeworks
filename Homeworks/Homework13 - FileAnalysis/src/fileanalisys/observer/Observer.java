package fileanalisys.observer;

import java.util.List;

/**
 * 30.09.2021
 * Homework13 - FileAnalysis
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface Observer {
    void update(List<String> list);
}
