package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 20.10.2021
 * 30. Java Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity(name="file_info")
public class FileInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="original_filename")
    private String originalFileName;
    @Column(name="storage_filename")
    private String storageFileName;
    private Long size;
    private String mimeType;
    private String description;
    @ManyToOne
    @JoinColumn(name="author_id", referencedColumnName = "id")
    private Account owner;
}
