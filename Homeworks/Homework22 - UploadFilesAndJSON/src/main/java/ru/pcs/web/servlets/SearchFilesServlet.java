package ru.pcs.web.servlets;

import org.springframework.context.ApplicationContext;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.services.FilesService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 21.10.2021
 * Homework22 - UploadFilesAndJSON
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@WebServlet("/searchFiles")
public class SearchFilesServlet extends HttpServlet {

    FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.filesService = springContext.getBean(FilesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // GET example localhost/searchFiles?fileName=73f90805-dc2a-43c3-b117-26e6b6da6cbd.jpg
        String storageFileName = request.getParameter("fileName");
        FileDto file = filesService.getFile(storageFileName);
        response.setContentType(file.getMimeType());
        response.setContentLength(file.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + file.getOriginalFileName() + "\"");
        filesService.writeFile(file, response.getOutputStream());
        response.flushBuffer();

//        request.getRequestDispatcher("/jsp/searchFiles.jsp").forward(request, response);

    }
}
