package ru.pcs.web.servlets;

import org.springframework.context.ApplicationContext;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.services.FilesService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * 20.10.2021
 * 30. Java Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/files")
public class FilesServlet extends HttpServlet {
    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.filesService = springContext.getBean(FilesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // получаем список ссылок с файлами
        HttpSession session = request.getSession();
        Long id = (Long) session.getAttribute("id");
        List<FileDto> filesByAuthorId = filesService.getFilesByAuthorId(id);

        if(!filesByAuthorId.isEmpty()){
        // выводим имена файлов
            request.setAttribute("files", filesByAuthorId);
        } else {
            // выведем сообщение, что список пуст
            request.setAttribute("no_files_message", "У вас нет загруженных файлов");
        }
        request.getRequestDispatcher("/jsp/files.jsp").forward(request, response);
    }
}
