package ru.pcs.web.servlets;

import org.springframework.context.ApplicationContext;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.services.FilesService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;

/**
 * 20.10.2021
 * 30. Java Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/filesUpload")
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext springContext = (ApplicationContext) servletContext.getAttribute("context");
        this.filesService = springContext.getBean(FilesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/jsp/filesUpload.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        BufferedReader descriptionReader = new BufferedReader(
                new InputStreamReader(request.getPart("description").getInputStream()));

        Part filePart = request.getPart("file");

        HttpSession session = request.getSession();
        Long id = (Long) session.getAttribute("id");

        // добавил в файл информацию об id пользователя, загрузившего его,
        // чтобы после можно было идентифицировать данного пользователя
        FileDto form = FileDto.builder()
                .description(descriptionReader.readLine())
                .fileName(filePart.getSubmittedFileName())
                .size(filePart.getSize())
                .mimeType(filePart.getContentType())
                .fileStream(filePart.getInputStream())
                .authorId(id)
                .build();

        filesService.upload(form);
    }
}
