package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.FileInfo;

import java.util.List;
import java.util.Optional;

/**
 * 02.11.2021
 * Homework22 - UploadFilesAndJSON
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface FileInfoRepository extends JpaRepository<FileInfo, Long> {

    Optional<FileInfo> findByStorageFileName(String storageFileName);

    List<FileInfo> findByOwnerId(Long ownerId);
}
