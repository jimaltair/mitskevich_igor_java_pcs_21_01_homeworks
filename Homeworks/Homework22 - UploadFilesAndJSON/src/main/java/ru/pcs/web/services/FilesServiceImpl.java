package ru.pcs.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.pcs.web.dto.FileDto;
import ru.pcs.web.models.Account;
import ru.pcs.web.models.FileInfo;
import ru.pcs.web.repositories.AccountsRepository;
import ru.pcs.web.repositories.FileInfoRepository;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * 20.10.2021
 * 30. Java Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class FilesServiceImpl implements FilesService {

    private final FileInfoRepository fileInfoRepository;
    private final AccountsRepository accountsRepository;
    @Value("${storage.path}")
    private String storagePath;

    @Autowired
    public FilesServiceImpl(FileInfoRepository fileInfoRepository, AccountsRepository accountsRepository) {
        this.fileInfoRepository = fileInfoRepository;
        this.accountsRepository = accountsRepository;
    }

    @Override
    public void upload(FileDto form) {
        String fileName = form.getFileName();
        String extension = fileName.substring(fileName.lastIndexOf("."));

        Account owner = accountsRepository.findAccountById(form.getAuthorId()).get();

        FileInfo fileInfo = FileInfo.builder()
                .originalFileName(fileName)
                .storageFileName(UUID.randomUUID() + extension)
                .description(form.getDescription())
                .mimeType(form.getMimeType())
                .size(form.getSize())
                .owner(owner)
                .build();

        fileInfoRepository.save(fileInfo);

        if (storagePath != null) {
            try {
                Files.copy(form.getFileStream(), Paths.get(storagePath + fileInfo.getStorageFileName()));
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    @Override
    public FileDto getFile(String storageFileName) {
        Optional<FileInfo> fileInfoOptional = fileInfoRepository.findByStorageFileName(storageFileName);
        if (fileInfoOptional.isPresent()) {
            FileDto file = FileDto.from(fileInfoOptional.get());
            file.setFileName(storageFileName);
            return file;
        }
        return null;
    }

    @Override
    public List<FileDto> getFilesByAuthorId(Long authorId) {
        List<FileDto> filesList = new ArrayList<>();
        List<FileInfo> filesByAuthorId = fileInfoRepository.findByOwnerId(authorId);
        for (FileInfo fileInfo : filesByAuthorId) {
            filesList.add(FileDto.from(fileInfo));
        }
        return filesList;
    }

    @Override
    public void writeFile(FileDto file, OutputStream outputStream) {
        try {
            Files.copy(Paths.get(storagePath + "\\" + file.getFileName()), outputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
