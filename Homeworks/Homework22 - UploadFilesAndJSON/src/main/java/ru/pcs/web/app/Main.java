package ru.pcs.web.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pcs.web.config.ApplicationConfig;
import ru.pcs.web.dto.SignUpForm;
import ru.pcs.web.repositories.AccountsRepository;
import ru.pcs.web.services.SignUpService;
import ru.pcs.web.services.SignUpServiceImpl;

import java.util.Properties;

/**
 * 18.10.2021
 * 30. Java Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        SignUpService signUpService = context.getBean(SignUpService.class);
        AccountsRepository accountsRepository = context.getBean(AccountsRepository.class);
        System.out.println(accountsRepository.findAccountByEmail("i.mitskevich@mail.ru").get());
//        System.out.println(accountsRepository.findAll());
    }
}
