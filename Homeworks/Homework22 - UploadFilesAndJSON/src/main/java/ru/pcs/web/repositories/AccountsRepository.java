package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 02.11.2021
 * Homework22 - UploadFilesAndJSON
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface AccountsRepository extends JpaRepository<Account, Long> {

    Optional<Account> findAccountById(Long id);

    Optional<Account> findAccountByEmail(String email);

    List<Account> findAccountsByEmail(String email);
}
