<%--
  Created by IntelliJ IDEA.
  User: jimal
  Date: 21.10.2021
  Time: 12:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search files</title>
</head>
<script>
    function searchFiles(originalFileName) {
        let request = new XMLHttpRequest();

        request.open('GET', '/files/json?originalFileName=' + originalFileName, false);
        request.send();

        request.onload = function () {
            if (request.status !== 200) {
                alert("Ошибка!")
            } else {
                let html =
                    '<tr>' +
                    '<th>Original filename</th>' +
                    '<th>Link</th>' +
                    '</tr>';

                let json = JSON.parse(request.response);

                for (let i = 0; i < json.length; i++) {
                    html += '<tr>'
                    html += '<td>' + json[i]['originalFileName'] + '</td>'
                    const url = "/files?fileName=" + json[i]['storageFileName'];
                    html += '<td><a href="' + url + '">' + json[i]['storageFileName'] + '</a></td>'
                    html += '</tr>'
                }

                document.getElementById('file_info').innerHTML = html;
            }
        }
    }
</script>
<body>
<label for="originalFileName">Search file by original filename:</label>
<input id="originalFileName" type="text" placeholder="Enter filename..."
       onkeyup="searchFiles(document.getElementById('originalFileName').value)">
<table id="file_info">
</table>
</body>
</html>
