<%--
  Created by IntelliJ IDEA.
  User: Marsel
  Date: 18.10.2021
  Time: 3:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profile</title>
</head>
<body>
<h1 style="color: ${color}">Profile</h1>
<p>First name: ${firstName}</p>
<p>Last name: ${lastName}</p>
<p>Email: ${email}</p>
<p>Password: ${password}</p>
<br>
<form method="link" action="LogoutServlet">
    <input type="submit" value="Logout"/>
</form>
<form method="link" action="FilesServlet">
    <input type="submit" value="Uploaded files"/>
</form>
<form method="link" action="SearchFilesServlet">
    <input type="submit" value="Search files"/>
</form>
</body>
</html>
