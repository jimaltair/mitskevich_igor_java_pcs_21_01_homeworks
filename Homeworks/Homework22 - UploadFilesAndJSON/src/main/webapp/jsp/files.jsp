<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: jimal
  Date: 20.10.2021
  Time: 19:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>upload files</title>
</head>
<body>
<h1>Uploaded files:</h1>
<table id="accounts_table">
    <tr>
    </tr>
    <c:forEach items="${files}" var="file">
        <tr>
            <td>${file.fileName}</td>
        </tr>
    </c:forEach>
</table>
<h3>${no_files_message}</h3>
</body>
</html>
