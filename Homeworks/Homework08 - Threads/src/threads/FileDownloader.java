package threads;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 17.09.2021
 * Homework08 - Threads
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class FileDownloader {

    private Path URLs;
    private static final String OUTPUT_DIRECTORY = "Homeworks/Homework08 - Threads/download/";
    private final AtomicInteger additionalIndex;

    public FileDownloader(String fileName) {
        this.URLs = Paths.get(fileName);
        additionalIndex = new AtomicInteger(0);
    }

    private String getReference() {
        try (Stream<String> lines = Files.lines(URLs)) {
            return lines.collect(Collectors.toList()).get(0);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void clearOutputDirectory() {
        File out = new File(OUTPUT_DIRECTORY);
        if (out.exists() && out.list().length != 0) {
            for (File file : out.listFiles()) {
                file.delete();
            }
        }
    }

    public long download() {
        String reference = getReference();
        URI uri = URI.create(reference);
        try (InputStream in = uri.toURL().openStream()) {

            String fileName = new File(uri.getPath()).getName();
            String fileNameWithoutExtension = fileName.substring(0, fileName.lastIndexOf('.'));
            String extension = fileName.substring(fileName.lastIndexOf('.'));

            synchronized (this) {
                if (Files.notExists(Paths.get(OUTPUT_DIRECTORY))) {
                    Files.createDirectory(Paths.get(OUTPUT_DIRECTORY));
                }
                Path downloadingFile = Paths.get(OUTPUT_DIRECTORY + fileName);
                if (Files.exists(downloadingFile)) {
                    int index = additionalIndex.getAndIncrement();
                    downloadingFile = Paths.get(OUTPUT_DIRECTORY + fileNameWithoutExtension + index + extension);
                }
                return Files.copy(in, downloadingFile);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
