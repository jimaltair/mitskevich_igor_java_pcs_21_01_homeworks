package threads;

import java.util.concurrent.*;

/**
 * 17.09.2021
 * Homework08 - Threads
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        String URLs = "Homeworks/Homework08 - Threads/lightweight_URL.txt";

        ExecutorService executor = Executors.newFixedThreadPool(12);

        FileDownloader downloader = new FileDownloader(URLs);
        downloader.clearOutputDirectory();

        FutureTask[] tasks = new FutureTask[100];

        for (int i = 0; i < 100; i++) {
            Callable<Long> callable = downloader::download;
            FutureTask<Long> future = (FutureTask<Long>) executor.submit(callable);
            tasks[i] = future;
        }

        long totalSize = 0L;

        int count = 0;
        while (count != tasks.length) {
            for (Future<?> future : tasks) {
                if (future.isDone()) {
                    count++;
                }
            }
        }
        for (FutureTask task : tasks) {
            try {
                long size = (long) task.get();
                totalSize += size;
            } catch (InterruptedException | ExecutionException e) {
                throw new IllegalArgumentException(e);
            }
        }
        System.out.println(totalSize);
        executor.shutdown();
    }

}
