## Homework08 - Threads

```
С помощью ExecutorService, Callable и Future реализовать многопоточную загрузку 100 файлов из интернета. В результате выполнения всех задач вывести их общий размер.

Ссылки на файлы для скачивания можно поместить в текстовый файл (разрешено использовать ссылку на один и тот же файл).

Приложить скрин VisualVM с реализованными потоками в папку проекта.

Количество потоков - 10-15.
```