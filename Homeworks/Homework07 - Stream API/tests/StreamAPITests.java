import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 16.09.2021
 * Homework07 - Stream API
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class StreamAPITests {
    String fileName = "Homeworks/Homework07 - Stream API/input.txt";
    CarRepository carRepository = new CarRepository(fileName);

    /**
     * Номера всех автомобилей, имеющих черный цвет или нулевой пробег
     */
    @Test
    public void findAllCarsWithBlackColorOrZeroMileage() {
        List<String> expected = Arrays.asList("Р454КЕ39", "У987МИ39", "Н289ЕО39");
        List<String> actual = carRepository.findAll().stream()
                .filter(car -> car.getColor().equals("black") || car.getMileage() == 0)
                .map(Car::getLicencePlate).collect(Collectors.toList());
        Assert.assertEquals(expected, actual);
    }

    /**
     * Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс
     */
    @Test
    public void countOfUniqModelsWithPriceFrom700To800Thousands() {
        long expected = 2;
        long actual = carRepository.findAll().stream()
                .filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                .map(Car::getModel)
                .distinct()
                .count();
        Assert.assertEquals(expected, actual);
    }

    /**
     * Вывести цвет автомобиля с минимальной стоимостью
     */
    @Test
    public void colorOfCarWithMinPrice() {
        String expected = "grey";
        String actual = carRepository.findAll().stream()
                .sorted(Comparator.comparingInt(Car::getPrice))
                .limit(1)
                .map(Car::getColor)
                .findAny()
                .get();
        Assert.assertEquals(expected, actual);
    }

    /**
     * Средняя стоимость Camry
     */
    @Test
    public void averagePriceOfCamry() {
        double expected = 1110330.0;
        double actual = carRepository.findAll().stream()
                .filter(car -> car.getModel().equals("Toyota Camry"))
                .mapToDouble(Car::getPrice)
                .average().getAsDouble();
        Assert.assertEquals(expected, actual, 0);
    }

}
