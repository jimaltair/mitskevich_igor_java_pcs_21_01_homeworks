import java.util.Comparator;

/**
 * 15.09.2021
 * Homework07 - Stream API
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {
        String fileName = "Homeworks/Homework07 - Stream API/input.txt";
        CarRepository carRepository = new CarRepository(fileName);

        /**
         * Номера всех автомобилей, имеющих черный цвет или нулевой пробег
         */
        carRepository.findAll().stream()
                .filter(car -> car.getColor().equals("black") || car.getMileage() == 0)
                .map(Car::getLicencePlate)
                .forEach(System.out::println);

        /**
         * Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс
         */
        long countOfUniqModelsWithPriceFrom700To800Thousands = carRepository.findAll().stream()
                .filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                .map(Car::getModel)
                .distinct()
                .count();
        System.out.println(countOfUniqModelsWithPriceFrom700To800Thousands);

        /**
         * Вывести цвет автомобиля с минимальной стоимостью
         */
        carRepository.findAll().stream()
                .sorted(Comparator.comparingInt(Car::getPrice))
                .limit(1)
                .map(Car::getColor).forEach(System.out::println);

        /**
         * Средняя стоимость Camry
         */
        carRepository.findAll().stream()
                .filter(car -> car.getModel().equals("Toyota Camry"))
                .mapToDouble(Car::getPrice)
                .average().ifPresent(System.out::println);
    }
}
