import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 15.09.2021
 * Homework07 - Stream API
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class CarRepository {

    private Path fileName;

    public CarRepository(String fileName) {
        this.fileName = Paths.get(fileName);
    }

    private final static Function<String, Car> carMapFunction = line -> {
        String[] parts = line.split("]\\[");
        String licencePlate = parts[0].substring(1);
        String model = parts[1];
        String color = parts[2];
        int mileage = Integer.parseInt(parts[3]);
        int price = Integer.parseInt(parts[4].substring(0, parts[4].length() - 1));
        return new Car(licencePlate, model, color, mileage, price);
    };

    public List<Car> findAll() {
        try (Stream<String> lines = Files.lines(fileName)) {
            return lines.map(carMapFunction).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
