import java.util.StringJoiner;

/**
 * 15.09.2021
 * Homework07 - Stream API
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Car {

    private String licencePlate;
    private String model;
    private String color;
    private int mileage;
    private int price;

    public Car(String licencePlate, String model, String color, int mileage, int price) {
        this.licencePlate = licencePlate;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("licencePlate='" + licencePlate + "'")
                .add("model='" + model + "'")
                .add("color='" + color + "'")
                .add("mileage=" + mileage)
                .add("price=" + price)
                .toString();
    }
}
