package ru.pcs.web.servlets;

import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.filters.ColorFilter;
import ru.pcs.web.services.AccountsService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 18.10.2021
 * 30. Java Web Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {

    private static final int COLOR_COOKIE_MAX_AGE = 60 * 60 * 24 * 365;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pageColor = request.getParameter("color");

        if (pageColor != null) {
            request.setAttribute("color", pageColor);
            Cookie cookie = new Cookie(ColorFilter.COLOR_COOKIE_NAME, pageColor);
            cookie.setMaxAge(COLOR_COOKIE_MAX_AGE);
            response.addCookie(cookie);
        }

        // получаем объект сессии, достаём из него все данные пользователя
        HttpSession session = request.getSession();
        String firstName = (String) session.getAttribute("firstName");
        String lastName = (String) session.getAttribute("lastName");
        String email = (String) session.getAttribute("email");
        String password = (String) session.getAttribute("password");

        // записываем все данные пользователя в запрос, для дальнейшей передачи в profile.jsp
        request.setAttribute("firstName", firstName);
        request.setAttribute("lastName", lastName);
        request.setAttribute("email", email);
        request.setAttribute("password", password);
        request.getRequestDispatcher("/jsp/profile.jsp").forward(request, response);
    }
}
