## Homework15 - JdbcRepository

```
Реализовать репозиторий для товаров с базовым набором операций (связи товара с другими сущностями реализовывать не нужно):

Optional<Product> findById(Long id);
List<Product> findAll(int page, int size);

void save(Product product);
void update(Product, product);

void delete(Product product);
void deleteById(Long id);
```