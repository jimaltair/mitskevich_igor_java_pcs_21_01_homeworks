package jdbcrepository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

/**
 * 05.10.2021
 * Homework15 - JdbcRepository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class HikariDataSourceInitializer {

    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "Trankpd1";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/online_shop";
    private static final String ORG_POSTGRESQL_DRIVER = "org.postgresql.Driver";
    private static int DEFAULT_POOL_SIZE = 10;
    private static final HikariConfig config;

    static {
        config = new HikariConfig();
        config.setUsername(DB_USER);
        config.setPassword(DB_PASSWORD);
        config.setDriverClassName(ORG_POSTGRESQL_DRIVER);
        config.setJdbcUrl(DB_URL);
        config.setMaximumPoolSize(DEFAULT_POOL_SIZE);
    }

    public static DataSource getHikariDataSource() {
        return new HikariDataSource(config);
    }

    public static void setMaximumPoolSize(int maxPoolSize) {
        config.setMaximumPoolSize(maxPoolSize);
    }
}
