package jdbcrepository;

import java.util.StringJoiner;

/**
 * 04.10.2021
 * Homework15 - JdbcRepository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Item {

    private Long id;
    private String name;
    private Double price;
    private Integer amount;

    public Item(Long id, String name, Double price, Integer amount) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.amount = amount;
    }

    public Item(String name, Double price, Integer amount) {
        this.name = name;
        this.price = price;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Item.class.getSimpleName() + "[", "]")
                .add("item_id=" + id)
                .add("name_item='" + name + "'")
                .add("price=" + price)
                .add("amount=" + amount)
                .toString();
    }
}
