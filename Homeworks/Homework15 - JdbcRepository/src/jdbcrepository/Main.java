package jdbcrepository;

import javax.sql.DataSource;

/**
 * 04.10.2021
 * Homework15 - JdbcRepository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {

        DataSource hikariDataSource = HikariDataSourceInitializer.getHikariDataSource();
        ItemRepository itemRepository = new ItemRepositoryJdbcImpl(hikariDataSource);

        /**
         * save
         */
        Item item1 = new Item("Item1", 10.0, 25);
        Item item2 = new Item("Item2", 20.0, 50);
        Item item3 = new Item("Item3", 30.0, 75);
        Item item4 = new Item("Item4", 40.0, 100);
        Item item5 = new Item("Item5", 50.0, 200);
        itemRepository.save(item1);
        itemRepository.save(item2);
        itemRepository.save(item3);
        itemRepository.save(item4);
        itemRepository.save(item5);

        /**
         * findById
         */
//        Item item1 = itemRepository.findById(1L).get();
//        Item item3 = itemRepository.findById(3L).get();
//        Item item5 = itemRepository.findById(5L).get();
//        System.out.println(item1);
//        System.out.println(item3);
//        System.out.println(item5);

        /**
         * findAll
         */
//        List<Item> items = itemRepository.findAll(0, 10);
//        System.out.println(items);

        /**
         * update
         */
//        itemRepository.update(new Item(1L, "Item1", 1000D, 1));

        /**
         * deleteById
         */
//        itemRepository.deleteById(1L);

        /**
         * delete
         */
//        itemRepository.delete(new Item("Item5", 50D, 200));
    }
}
