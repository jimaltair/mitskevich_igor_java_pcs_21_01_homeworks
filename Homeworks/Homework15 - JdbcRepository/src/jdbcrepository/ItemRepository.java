package jdbcrepository;

import java.util.List;
import java.util.Optional;

/**
 * 04.10.2021
 * Homework15 - JdbcRepository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface ItemRepository {

    Optional<Item> findById(Long id);

    List<Item> findAll(int page, int size);

    void save(Item item);

    void update(Item item);

    void delete(Item item);

    void deleteById(Long id);
}
