package jdbcrepository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * 04.10.2021
 * Homework15 - JdbcRepository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class ItemRepositoryJdbcImpl implements ItemRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select item_id, name_item, price, amount from item order by item_id limit ? offset ?;";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select item_id, name_item, price, amount from item where item_id = ?";

    //language=SQL
    private static final String SQL_INSERT = "insert into item(name_item, price, amount) values (?, ?, ?)";

    //language=SQL
    private static final String SQL_UPDATE = "update item set name_item = ?, price = ?, amount = ? where item_id = ?;";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from item where item_id = ?;";

    //language=SQL
    private static final String SQL_DELETE_BY_ITEM = "delete from item where name_item = ? and price = ? and amount = ?;";

    private final DataSource dataSource;

    private static final Function<ResultSet, Item> itemMapper = resultSet -> {
        try {
            Long id = resultSet.getLong(1);
            String name = resultSet.getString(2);
            Double price = resultSet.getDouble(3);
            Integer amount = resultSet.getInt(4);
            return new Item(id, name, price, amount);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    public ItemRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Optional<Item> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {

            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(itemMapper.apply(resultSet));
                }
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Item> findAll(int page, int size) {
        List<Item> items = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)) {

            statement.setInt(1, size);
            statement.setInt(2, page * size);

            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    items.add(itemMapper.apply(resultSet));
                }
                return items;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void save(Item item) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, item.getName());
            statement.setDouble(2, item.getPrice());
            statement.setInt(3, item.getAmount());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert item");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Item item) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {

            statement.setString(1, item.getName());
            statement.setDouble(2, item.getPrice());
            statement.setInt(3, item.getAmount());
            statement.setLong(4, item.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update item");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(Item item) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ITEM)) {

            statement.setString(1, item.getName());
            statement.setDouble(2, item.getPrice());
            statement.setInt(3, item.getAmount());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't delete item");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void deleteById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_BY_ID)) {
            statement.setLong(1, id);
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't delete item");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
