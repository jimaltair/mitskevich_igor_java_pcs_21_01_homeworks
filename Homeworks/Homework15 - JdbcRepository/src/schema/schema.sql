CREATE TABLE item (
                      item_id BIGSERIAL NOT NULL,
                      name_item VARCHAR(50),
                      price DECIMAL(8,2),
                      amount INTEGER
);


ALTER TABLE item ADD CONSTRAINT item_pkey PRIMARY KEY (item_id);