## Homework14 - StartDB

```
Спроектировать базу данных интернет-магазина, sql-скрипт создания таблиц разместить в файле schema.sql

- Товар
- Покупатель
- Заказ
- Бронь товара
- Бронь заказа
- Чек

Содержимое таблиц на усмотрение учащегося.

Также придумать и написать два "сложных" запроса к бд с указанием комментария, что именно делает этот запрос.
```