package except.usersservice;

import except.exceptions.BadEmailException;
import except.exceptions.BadPasswordException;
import except.exceptions.UserNotFoundException;
import except.user.User;

import java.util.ArrayList;
import java.util.List;


/**
 * 10.09.2021
 * Homework05 - Exceptions
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class UsersServiceImpl implements UsersService {

    private List<User> usersList;

    public UsersServiceImpl() {
        usersList = new ArrayList<>();
    }

    public List<User> getUsersList() {
        return usersList;
    }

    public boolean isPasswordValid(String password) {
        if (password.length() < 8) return false;
        boolean isValid = true;
        boolean haveAtLeastOneLetter = false;
        boolean haveAtLeastOneDigit = false;
        for (char aChar : password.toCharArray()) {
            if (Character.isLetter(aChar)) {
                haveAtLeastOneLetter = true;
            } else if (Character.isDigit(aChar)) {
                haveAtLeastOneDigit = true;
            } else {
                isValid = false;
            }
        }
        return (isValid && haveAtLeastOneLetter && haveAtLeastOneDigit);
    }

    public boolean isEmailValid(String email) {
        long occurrencesCount = email.chars().filter(ch -> ch == '@').count();
        return occurrencesCount == 1;
    }

    @Override
    public void signUp(String email, String password) {
        if (!isEmailValid(email)) {
            throw new BadEmailException("Адрес электронной почты должен содержать символ '@'");
        }
        if (!isPasswordValid(password)) {
            throw new BadPasswordException("Пароль должен содержать только буквы и цифры и быть " +
                    "длиной не менее 8 символов");
        }
        User user = new User(email, password);
        usersList.add(user);
        System.out.println("В БД был добавлен пользователь " + user);
    }

    @Override
    public void signIn(String email, String password) {
        User user = new User(email, password);
        for (User user1 : usersList) {
            if (user.equals(user1)) {
                System.out.println("В БД был найден пользователь " + user);
                return;
            }
        }
        throw new UserNotFoundException("В БД пользователь " + user + " не найден");
    }
}
