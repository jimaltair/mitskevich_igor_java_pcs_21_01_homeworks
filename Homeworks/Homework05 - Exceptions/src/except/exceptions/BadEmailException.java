package except.exceptions;

/**
 * 10.09.2021
 * Homework05 - Exceptions
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class BadEmailException extends RuntimeException {

    public BadEmailException() {
    }

    public BadEmailException(String message){
        super(message);
    }
}
