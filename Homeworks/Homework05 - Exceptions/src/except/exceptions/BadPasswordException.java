package except.exceptions;

/**
 * 10.09.2021
 * Homework05 - Exceptions
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class BadPasswordException extends RuntimeException {

    public BadPasswordException() {
    }

    public BadPasswordException(String message) {
        super(message);
    }
}
