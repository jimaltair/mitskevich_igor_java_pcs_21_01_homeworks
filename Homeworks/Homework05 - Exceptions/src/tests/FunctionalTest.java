package tests;

import except.exceptions.BadEmailException;
import except.exceptions.BadPasswordException;
import except.exceptions.UserNotFoundException;
import except.user.User;
import except.usersservice.UsersServiceImpl;
import org.junit.Assert;
import org.junit.Test;


/**
 * 11.09.2021
 * Homework05 - Exceptions
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class FunctionalTest {

    private UsersServiceImpl service = new UsersServiceImpl();

    private User user1 = new User("abcd@gmail.com", "12345k678");
    private User user2 = new User("321agt@mail.com", "TK54JHWda9");
    private User user3 = new User("torchoa@rambler.ru", "1cuscsua1");

    public void createTestData() {
        service.signUp(user1.getEmail(), user1.getPassword());
        service.signUp(user2.getEmail(), user2.getPassword());
        service.signUp(user3.getEmail(), user3.getPassword());
    }

    // проверка правильности сохранения пользователей внутри UsersServiceImpl
    @Test
    public void signUpAllUsers() {

        UsersServiceImpl service1 = new UsersServiceImpl();

        service1.signUp(user1.getEmail(), user1.getPassword());
        service1.signUp(user2.getEmail(), user2.getPassword());
        service1.signUp(user3.getEmail(), user3.getPassword());

        createTestData();
        Assert.assertEquals(service.getUsersList(), service1.getUsersList());
    }

    // проверка на null
    @Test
    public void getAllUsers_NO_NULL() {
        createTestData();
        Assert.assertNotNull(service.getUsersList().get(0));
        Assert.assertNotNull(service.getUsersList().get(1));
        Assert.assertNotNull(service.getUsersList().get(2));
    }

    // почта без символа '@'
    @Test(expected = BadEmailException.class)
    public void testSignUpWithInvalidEmailOne() {
        service.signUp("abcd12345.com", "12345k678");
    }

    // почта с двумя символами '@'
    @Test(expected = BadEmailException.class)
    public void testSignUpWithInvalidEmailTwo() {
        service.signUp("abcd@@12345.com", "12345k678");
    }

    // пароль, состоящий только из цифр
    @Test(expected = BadPasswordException.class)
    public void testSignUpWithInvalidPasswordOne() {
        service.signUp("abcd@gmail.com", "123456789");
    }

    // пароль, состоящий только из букв
    @Test(expected = BadPasswordException.class)
    public void testSignUpWithInvalidPasswordTwo() {
        service.signUp("abcd@gmail.com", "zzzAAAkkklll");
    }

    // пароль длиной 7 символов
    @Test(expected = BadPasswordException.class)
    public void testSignUpWithInvalidPasswordThree() {
        service.signUp("abcd@gmail.com", "12ab12C");
    }

    // пароль имеет в составе символ, отличный от буквы или цифры
    @Test(expected = BadPasswordException.class)
    public void testSignUpWithInvalidPasswordFour() {
        service.signUp("abcd@gmail.com", "12345_k678");
    }

    // искомый user совпадает только по адресу почты
    @Test(expected = UserNotFoundException.class)
    public void testSignInWithNotExistingUser() {
        createTestData();
        service.signIn("abcd@gmail.com", "zzz123zzz");
    }

}
