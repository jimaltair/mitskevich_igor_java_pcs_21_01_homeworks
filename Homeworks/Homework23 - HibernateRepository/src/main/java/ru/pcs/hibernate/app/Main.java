package ru.pcs.hibernate.app;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.hibernate.models.FileInfo;
import ru.pcs.hibernate.repository.FilesRepositoryHibernateImpl;
import ru.pcs.hibernate.services.SessionFactoryService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * 22.10.2021
 * Homework23 - HibernateRepository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {

    private static String fileName = "src\\main\\resources\\istockphoto-1300278981-170667a.jpg";

    public static void main(String[] args) {

        SessionFactory sessionFactory = SessionFactoryService.getSessionFactory();
        FilesRepositoryHibernateImpl filesRepository = new FilesRepositoryHibernateImpl(sessionFactory);
        String storageFileName = filesRepository.saveFile(fileName);

        Path path = filesRepository.loadFile(storageFileName);
        System.out.println("------------------------------------------------------------");
        System.out.println("Файл загружен успешно: " + path);
        sessionFactory.close();
    }
}
