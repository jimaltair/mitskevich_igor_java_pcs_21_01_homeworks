package ru.pcs.hibernate.repository;

import java.nio.file.Path;

/**
 * 23.10.2021
 * Homework23 - HibernateRepository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface FilesRepository {
    String saveFile(String fileName);

    Path loadFile(String fileName);
}
