package ru.pcs.hibernate.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FilenameUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * 22.10.2021
 * Homework23 - HibernateRepository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class FileInfo {

    private String originalFileName;
    private String storageFileName;
    private Long size;

    public static FileInfo get(String fileName){

        String originalFileName = FilenameUtils.getName(fileName);
        String extension = FilenameUtils.getExtension(fileName);
        String storageFileName = UUID.randomUUID() + "." + extension;

        long size;
        try {
            size = Files.size(Paths.get(fileName));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return FileInfo.builder()
                .originalFileName(originalFileName)
                .storageFileName(storageFileName)
                .size(size)
                .build();
    }
}
