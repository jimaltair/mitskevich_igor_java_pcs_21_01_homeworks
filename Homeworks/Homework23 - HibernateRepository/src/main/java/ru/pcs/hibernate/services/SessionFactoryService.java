package ru.pcs.hibernate.services;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * 26.10.2021
 * Homework23 - HibernateRepository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class SessionFactoryService {

    private static final String HIBERNATE_CONFIG = "hibernate\\hibernate.cfg.xml";

    public static SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure(HIBERNATE_CONFIG);
        return configuration.buildSessionFactory();
    }
}
