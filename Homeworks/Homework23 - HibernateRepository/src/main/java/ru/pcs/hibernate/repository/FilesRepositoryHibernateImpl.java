package ru.pcs.hibernate.repository;

import org.apache.commons.io.FilenameUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.pcs.hibernate.models.FileInfo;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * 23.10.2021
 * Homework23 - HibernateRepository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class FilesRepositoryHibernateImpl implements FilesRepository {

    private static final String STORAGE = "src\\main\\resources\\storage\\";
    private SessionFactory sessionFactory;

    public FilesRepositoryHibernateImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public String saveFile(String fileName) {

        Session session = sessionFactory.openSession();
        FileInfo fileInfo = FileInfo.get(fileName);

        try {
            createStorage();
            Files.copy(Paths.get(fileName), Paths.get(STORAGE + fileInfo.getStorageFileName()));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        session.beginTransaction();
        session.persist(fileInfo);
        session.getTransaction().commit();
        return fileInfo.getStorageFileName();
    }

    @Override
    public Path loadFile(String fileName) {

        Session session = sessionFactory.openSession();
        FileInfo fileInfo = session.get(FileInfo.class, fileName);
        session.close();

        return Paths.get(fileInfo.getStorageFileName());
    }

    private static void createStorage() throws IOException {
        Path storage = Paths.get(STORAGE);
        if (!storage.toFile().exists()) {
            Files.createDirectory(storage);
        }
    }
}
