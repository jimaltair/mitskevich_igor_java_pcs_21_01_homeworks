package ru.psc.jpa.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FilenameUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * 25.10.2021
 * Homework24 - Jpa Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
public class FileInfo {

    @Id
    @Column(name = "storage_filename")
    private String storageFileName;
    @Column(name = "original_filename")
    private String originalFileName;
    private Long size;

    public static FileInfo get(String fileName) {

        String originalFileName = FilenameUtils.getName(fileName);
        String extension = FilenameUtils.getExtension(fileName);
        String storageFileName = UUID.randomUUID() + "." + extension;

        long size;
        try {
            size = Files.size(Paths.get(fileName));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return FileInfo.builder()
                .originalFileName(originalFileName)
                .storageFileName(storageFileName)
                .size(size)
                .build();
    }
}
