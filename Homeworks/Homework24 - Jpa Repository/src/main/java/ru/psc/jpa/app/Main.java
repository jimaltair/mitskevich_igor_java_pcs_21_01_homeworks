package ru.psc.jpa.app;

import ru.psc.jpa.repositories.FilesRepositoryJpaImpl;
import ru.psc.jpa.services.EntityManagerService;

import javax.persistence.EntityManager;
import java.nio.file.Path;

/**
 * 25.10.2021
 * Homework24 - Jpa Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {

    private static String fileName = "src\\main\\resources\\webimage-6B9B966C-632E-4C3C-9A4D0E2E5B4A6E0E.jpg";

    public static void main(String[] args) {

        EntityManager entityManager = EntityManagerService.getEntityManager();

        FilesRepositoryJpaImpl filesRepository = new FilesRepositoryJpaImpl(entityManager);
        String storageFileName = filesRepository.saveFile(fileName);

        Path path = filesRepository.loadFile(storageFileName);
        System.out.println("------------------------------------------------------------");
        System.out.println("Файл загружен успешно: " + path);
    }
}
