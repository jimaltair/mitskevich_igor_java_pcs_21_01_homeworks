package ru.psc.jpa.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.psc.jpa.models.FileInfo;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 25.10.2021
 * Homework24 - Jpa Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class FilesRepositoryJpaImpl implements FileRepository {

    private static final String STORAGE = "src\\main\\resources\\storage\\";
    private EntityManager entityManager;

    public FilesRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * сохраняем файл на локальный диск
     * @param fileName - имя файла, который мы собираемся сохранить
     * @return storageFileName - уникальное имя файла в хранилище, по которому его впоследствии можно будет идентифицировать
     */
    @Override
    public String saveFile(String fileName) {

        EntityTransaction transaction = entityManager.getTransaction();

        FileInfo fileInfo = FileInfo.get(fileName);

        try {
            createStorage();
            Files.copy(Paths.get(fileName), Paths.get(STORAGE + fileInfo.getStorageFileName()));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        transaction.begin();
        entityManager.persist(fileInfo);
        transaction.commit();

        return fileInfo.getStorageFileName();
    }

    private static void createStorage() throws IOException {
        Path storage = Paths.get(STORAGE);
        if (!storage.toFile().exists()) {
            Files.createDirectory(storage);
        }
    }

    /**
     * загружаем файл из локального хранилища
     * @param fileName - уникальное имя файла в хранилище, по которому мы можем его идентифицировать
     * @return объект Path, ассоциированный с искомым файлом
     */
    @Override
    public Path loadFile(String fileName) {
        TypedQuery<FileInfo> query = entityManager.createQuery("select fileinfo from FileInfo fileinfo where " +
                "fileinfo.storageFileName = :filename", FileInfo.class);
        query.setParameter("filename", fileName);
        return Paths.get(query.getSingleResult().getStorageFileName());
    }
}
