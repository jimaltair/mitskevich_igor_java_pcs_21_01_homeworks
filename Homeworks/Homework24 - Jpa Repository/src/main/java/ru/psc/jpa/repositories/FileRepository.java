package ru.psc.jpa.repositories;

import java.nio.file.Path;

/**
 * 25.10.2021
 * Homework24 - Jpa Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface FileRepository {
    String saveFile(String fileName);

    Path loadFile(String fileName);
}
