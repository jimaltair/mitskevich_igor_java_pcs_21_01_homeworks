package ru.psc.jpa.services;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;

/**
 * 26.10.2021
 * Homework24 - Jpa Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class EntityManagerService {

    private static final String HIBERNATE_CONFIG = "hibernate\\hibernate.cfg.xml";

    public static EntityManager getEntityManager() {
        Configuration configuration = new Configuration();
        configuration.configure(HIBERNATE_CONFIG);
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        return sessionFactory.createEntityManager();
    }
}
