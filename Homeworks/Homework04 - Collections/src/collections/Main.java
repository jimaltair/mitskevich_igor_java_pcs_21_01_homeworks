package collections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * 08.09.2021
 * Homework04 - Collections
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {

        String input;
        Map<Character, Integer> map = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Введите строку:");
            while ((input = reader.readLine()) != null) {
                for (char aChar : input.toCharArray()) {
                    if (!Character.isLetter(aChar)) continue;
                    if (map.containsKey(aChar)) {
                        map.put(aChar, map.get(aChar) + 1);
                    } else {
                        map.put(aChar, 1);
                    }
                }
                System.out.println("\nКоличество букв, встречающихся в данной строке:");
                for (Character character : map.keySet()) {
                    System.out.println(character + " - " + map.get(character));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
