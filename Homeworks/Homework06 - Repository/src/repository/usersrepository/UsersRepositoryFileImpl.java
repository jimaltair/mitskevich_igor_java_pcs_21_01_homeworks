package repository.usersrepository;

import repository.idgenerator.IdGenerator;
import repository.user.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 13.09.2021
 * Homework06 - Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class UsersRepositoryFileImpl implements UsersRepository {
    private final String fileName;
    private final IdGenerator idGenerator;

    public UsersRepositoryFileImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String input;
            while ((input = reader.readLine()) != null) {
                users.add(getUserFromString(input));
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    private User getUserFromString(String input) {
        String[] userData = input.split("[|]");
        User user = new User(userData[1], userData[2]);
        user.setId(Integer.parseInt(userData[0]));
        return user;
    }

    /**
     * Если в файле уже есть такой пользователь, у него нужно обновить данные,
     * т.е. перезаписать данные пользователя в файле
     */
    @Override
    public void update(User user) {
        File sourceFile = new File(fileName);
        File outputFile;
        try {
            outputFile = File.createTempFile(fileName, null);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
             BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {
            String input;
            while ((input = reader.readLine()) != null) {
                User userFromString = getUserFromString(input);
                if (!user.getId().equals(userFromString.getId())) {
                    writer.write(input);
                    writer.newLine();
                } else {
                    String userAsLine = user.getId() + "|" + user.getEmail() + "|" + user.getPassword();
                    writer.write(userAsLine);
                    writer.newLine();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        sourceFile.delete();
        outputFile.renameTo(sourceFile);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return findAll()
                .stream()
                .filter(user -> user.getEmail().equals(email))
                .findAny();
    }

    @Override
    public void delete(User user) {
        File sourceFile = new File(fileName);
        File outputFile;
        try {
            outputFile = File.createTempFile(fileName, null);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(sourceFile));
             BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {
            String input;
            while ((input = reader.readLine()) != null) {
                User userFromString = getUserFromString(input);
                if (!user.equals(userFromString)) {
                    writer.write(input);
                    writer.newLine();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        sourceFile.delete();
        outputFile.renameTo(sourceFile);
    }

    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            user.setId(idGenerator.next());
            String userAsLine = user.getId() + "|" + user.getEmail() + "|" + user.getPassword();
            writer.write(userAsLine);
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public int count() {
        return findAll().size();
    }

    @Override
    public boolean existsByEmail(String email) {
        return findAll()
                .stream()
                .anyMatch(user -> user.getEmail().equals(email));
    }
}
