package repository.usersrepository;

import repository.user.User;

import java.util.List;
import java.util.Optional;

/**
 * 13.09.2021
 * Homework06 - Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface UsersRepository {

    void update(User user);
    Optional<User> findByEmail(String email);
    List<User> findAll();
    void delete(User user);
    void save(User user);
    int count();
    boolean existsByEmail(String email);

}
