package repository.idgenerator;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * 13.09.2021
 * Homework06 - Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class IdGeneratorFileImpl implements IdGenerator {

    private final String fileName;

    public IdGeneratorFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public boolean hasNext() {
        return true;
    }

    @Override
    public Integer next() {
        try (Scanner scanner = new Scanner(new FileInputStream(fileName))) {
            int lastId = scanner.nextInt();
            lastId++;
            scanner.close();
            try (PrintWriter printWriter = new PrintWriter(new FileWriter(fileName))) {
                printWriter.print(lastId);
            }
            return lastId;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
