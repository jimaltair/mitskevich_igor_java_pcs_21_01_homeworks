package repository.idgenerator;

import java.util.Iterator;

/**
 * 13.09.2021
 * Homework06 - Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface IdGenerator extends Iterator<Integer> {
}
