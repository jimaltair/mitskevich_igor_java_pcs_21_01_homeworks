package repository;

import repository.idgenerator.IdGenerator;
import repository.idgenerator.IdGeneratorFileImpl;
import repository.user.User;
import repository.usersrepository.UsersRepositoryFileImpl;
import repository.usersservice.UsersServiceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 13.09.2021
 * Homework06 - Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {

        IdGenerator idGenerator = new IdGeneratorFileImpl("Homeworks/Homework06 - Repository/users_id.txt");
        UsersRepositoryFileImpl usersRepository = new UsersRepositoryFileImpl("Homeworks/Homework06 - Repository/users.txt", idGenerator);
        UsersServiceImpl usersService = new UsersServiceImpl(usersRepository);

        /* Данные пользователей, лежащие в users.txt
        1|user1@mail.ru|password1
        2|user2@mail.ru|password2
        3|user3@mail.ru|password3
        4|email4@mail.ru|password4
         */

        /**
         * вводим данные новых пользователей, для выхода набрать exit
         */
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String email = reader.readLine();
                if (email.equals("exit")) return;
                String password = reader.readLine();
                if (password.equals("exit")) return;
                usersService.signUp(email, password);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
         * количество зарегистрированных пользователей
         */
        System.out.println(usersService.getUsersCount());
        System.out.println("--------------------------------------------");
        /**
         * находим пользователя по email
         */
        System.out.println(usersService.findUserByEmail("email@mail.ru")); // несуществующий пользователь
        System.out.println(usersService.findUserByEmail("email4@mail.ru")); // существующий пользователь
        System.out.println("--------------------------------------------");

        /**
         * получаем список пользователей
         */
        System.out.println(usersService.findAllUsers().toString());

        /**
         * аутентифицируем существующего пользователя
         */
        usersService.signIn("user3@mail.ru", "password3");

        /**
         * аутентифицируем несуществующего пользователя
         */
        usersService.signIn("user345@mail.ru", "password3");

        /**
         * Обновляем данные пользователя
         */
        User testUser1 = new User("xxxMegaUserxxx@yandex.ru", "SuPeRpAsSwOrD123");
        testUser1.setId(4);
        usersService.update(testUser1);

        /**
         * Удаляем пользователя
         */
        User testUser2 = new User("user3@mail.ru", "password3");
        usersService.delete(testUser2);
    }

}
