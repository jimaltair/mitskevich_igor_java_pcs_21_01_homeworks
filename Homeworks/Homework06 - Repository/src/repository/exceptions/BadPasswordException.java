package repository.exceptions;

/**
 * 13.09.2021
 * Homework06 - Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class BadPasswordException extends RuntimeException {

    public BadPasswordException() {
    }

    public BadPasswordException(String message) {
        super(message);
    }
}
