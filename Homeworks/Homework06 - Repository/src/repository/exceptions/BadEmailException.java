package repository.exceptions;

/**
 * 13.09.2021
 * Homework06 - Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class BadEmailException extends RuntimeException {

    public BadEmailException() {
    }

    public BadEmailException(String message){
        super(message);
    }
}
