package repository.usersservice;

import repository.exceptions.BadEmailException;
import repository.exceptions.BadPasswordException;
import repository.exceptions.UserNotFoundException;
import repository.user.User;
import repository.usersrepository.UsersRepository;

import java.util.List;
import java.util.Optional;


/**
 * 13.09.2021
 * Homework06 - Repository
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }


    public boolean isPasswordValid(String password) {
        if (password.length() < 8) return false;
        boolean isValid = true;
        boolean haveAtLeastOneLetter = false;
        boolean haveAtLeastOneDigit = false;
        for (char aChar : password.toCharArray()) {
            if (Character.isLetter(aChar)) {
                haveAtLeastOneLetter = true;
            } else if (Character.isDigit(aChar)) {
                haveAtLeastOneDigit = true;
            } else {
                isValid = false;
            }
        }
        return (isValid && haveAtLeastOneLetter && haveAtLeastOneDigit);
    }

    public List<User> findAllUsers() {
        return usersRepository.findAll();
    }

    public void update(User user) {
        usersRepository.update(user);
    }

    public boolean isEmailValid(String email) {
        long occurrencesCount = email.chars().filter(ch -> ch == '@').count();
        return occurrencesCount == 1;
    }

    public Optional<User> findUserByEmail(String email) {
        return usersRepository.findByEmail(email);
    }

    public void delete(User user) {
        usersRepository.delete(user);
    }

    public int getUsersCount(){
        return usersRepository.count();
    }

    // сохранение нового пользователя
    @Override
    public void signUp(String email, String password) {
        if (!isEmailValid(email)) {
            throw new BadEmailException("Адрес электронной почты должен содержать символ '@'");
        }
        if (!isPasswordValid(password)) {
            throw new BadPasswordException("Пароль должен содержать только буквы и цифры и быть " +
                    "длиной не менее 8 символов");
        }

        if (!usersRepository.existsByEmail(email)) {
            User user = new User(email, password);
            usersRepository.save(user);
            System.out.println("Добавлен новый пользователь " + user);
        } else {
            throw new IllegalArgumentException("Такой email уже существует");
        }
    }

    // аутентификация существующего пользователя
    @Override
    public void signIn(String email, String password) {
        usersRepository.findByEmail(email).orElseThrow(() ->
                new UserNotFoundException("Пользователь не найден"));

    }

}
