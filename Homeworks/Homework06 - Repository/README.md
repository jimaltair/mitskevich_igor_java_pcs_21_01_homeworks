## Homework06 - Repository

```
Используя код Homework05, добавить в UsersService реализацию репозитория UsersRepository вместо  списка.

Реализовать все методы UsersRepository на основе файлового ввода-вывода (см. код занятия):

public interface UsersRepository {

    void update(User user);
    Optional<User> findByEmail(String email);
    List<User> findAll();
    void delete(User user);
    void save(User user);
    int count();
    boolean existsByEmail(String email);

}
```