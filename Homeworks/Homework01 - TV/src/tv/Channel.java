package tv;

import java.util.ArrayList;
import java.util.Random;

/**
 * 06.09.2021
 * Homework01-TV
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Channel {

    private String name;
    private ArrayList<Program> programs;

    public Channel(String name) {
        this.name = name;
        programs = new ArrayList<>();
    }

    public void addProgram(Program program) {
        programs.add(program);
    }

    public Program getRandomProgram() {
        Random random = new Random();
        return programs.get(random.nextInt(programs.size()));
    }
}
