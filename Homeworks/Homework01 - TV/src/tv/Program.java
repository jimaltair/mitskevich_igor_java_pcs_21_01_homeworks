package tv;

/**
 * 06.09.2021
 * Homework01-TV
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Program {

    private String name;

    public Program(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
