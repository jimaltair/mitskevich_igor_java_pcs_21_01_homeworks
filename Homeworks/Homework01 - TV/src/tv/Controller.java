package tv;

/**
 * 06.09.2021
 * Homework01-TV
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Controller {

    private TV tv;

    public Controller(TV tv) {
        this.tv = tv;
    }

    public void on(int i) {
        Channel channel = tv.getChannel(i);
        Program program = channel.getRandomProgram();
        System.out.println("Выбрана программа: " + program.getName());
    }
}
