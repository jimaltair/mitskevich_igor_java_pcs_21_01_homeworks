package tv;

import java.util.ArrayList;

/**
 * 06.09.2021
 * Homework01-TV
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class TV {

    private String model;
    private ArrayList<Channel> channels;

    public TV(String model) {
        this.model = model;
        channels = new ArrayList<>();
    }

    public void addChannel(Channel channel) {
        channels.add(channel);
    }

    public Channel getChannel(int i) {
        // т.к. в телевизоре нумерация каналов начинается с 1, а в ArrayList c 0-го индекса, мы получаем канал с индексом i - 1
        return channels.get(i - 1);
    }
}

