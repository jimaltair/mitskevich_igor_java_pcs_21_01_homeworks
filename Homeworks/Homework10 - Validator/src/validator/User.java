package validator;

import validator.annotations.Max;
import validator.annotations.Min;
import validator.annotations.MinMaxLength;
import validator.annotations.NotEmpty;

/**
 * 22.09.2021
 * Homework10 - Validator
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class User {

    @NotEmpty
    @MinMaxLength(min = 2, max = 16)
    private String name;
    @Min(value = 14)
    @Max(value = 99)
    private int age;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public User(int age) {
        this.age = age;
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
