package validator;

import validator.annotations.AnnotationInitializer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * 22.09.2021
 * Homework10 - Validator
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Validator {

    public static void validate(Object form) {

        Class<?> formClass = form.getClass();
        Field[] fields = formClass.getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            for (Annotation annotation : field.getAnnotations()) {
                AnnotationInitializer initializer = new AnnotationInitializer(form, field, annotation);
                initializer.initialize();
            }
        }
    }
}
