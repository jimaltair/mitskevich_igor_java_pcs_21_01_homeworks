package validator;

/**
 * 22.09.2021
 * Homework10 - Validator
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
/**
 * проверка @NotEmpty
 */
        User user1 = new User(30);
        Validator.validate(user1);
/**
 * проверка @Min
 */
//        User user2 = new User("Василий", 13);
//        Validator.validate(user2);
/**
 * проверка @Max
 */
//        User user3 = new User("Василий", 125);
//        Validator.validate(user3);
/**
 * проверка @MinMaxLength
 */
//        User user4 = new User("A", 5);
//        Validator.validate(user4);

    }
}
