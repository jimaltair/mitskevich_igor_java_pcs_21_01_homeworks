package validator.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * 23.09.2021
 * Homework07 - Stream API
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class AnnotationInitializer {

    private Object form;
    private Field field;
    private Annotation annotation;

    public AnnotationInitializer(Object form, Field field, Annotation annotation) {
        this.form = form;
        this.field = field;
        this.annotation = annotation;
    }

    public void initialize() {
        switch (annotation.annotationType().getName()) {
            case "validator.annotations.NotEmpty":
                initializeNotEmptyAnnotation();
                break;
            case "validator.annotations.Min":
                initializeMinAnnotation();
                break;
            case "validator.annotations.Max":
                initializeMaxAnnotation();
                break;
            case "validator.annotations.MinMaxLength":
                initializeMinMaxAnnotation();
                break;
        }
    }

    private void initializeNotEmptyAnnotation() {
        try {
            if (field.get(form) == null)
                throw new IllegalArgumentException("Поле " + field.getName() + " не можем быть пустым");
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void initializeMinAnnotation() {
        Min min = field.getAnnotation(Min.class);
        try {
            if ((int) field.get(form) < min.value())
                throw new IllegalArgumentException("Значение поля " + field.getName() +
                        " не можем быть меньше " + min.value());
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void initializeMaxAnnotation() {
        Max max = field.getAnnotation(Max.class);
        try {
            if ((int) field.get(form) > max.value())
                throw new IllegalArgumentException("Значение поля " + field.getName() +
                        " не можем быть больше " + max.value());
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void initializeMinMaxAnnotation() {
        MinMaxLength minMaxLength = field.getAnnotation(MinMaxLength.class);
        try {
            int length = String.valueOf(field.get(form)).length();
            if (length < minMaxLength.min() || length > minMaxLength.max())
                throw new IllegalArgumentException("Длина значения поля " + field.getName() +
                        " должна быть в диапазоне от " + minMaxLength.min() + " до " + minMaxLength.max());
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
