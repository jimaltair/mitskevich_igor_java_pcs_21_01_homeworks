package ru.pcs.web.services;

import ru.pcs.web.models.Token;

import java.util.List;

/**
 * 02.12.2021
 * Attestation02 - JWT + Mongo
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface BlackListTokensService {
    void addToken(String token);

    boolean findToken(String token);
}
