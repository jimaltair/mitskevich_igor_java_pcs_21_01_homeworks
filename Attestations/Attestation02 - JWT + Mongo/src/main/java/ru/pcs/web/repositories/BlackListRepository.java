package ru.pcs.web.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import ru.pcs.web.models.Token;

import java.util.List;

/**
 * 02.12.2021
 * Attestation02 - JWT + Mongo
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface BlackListRepository extends MongoRepository<Token, String> {
    Token findTokenByToken(String token);
}
