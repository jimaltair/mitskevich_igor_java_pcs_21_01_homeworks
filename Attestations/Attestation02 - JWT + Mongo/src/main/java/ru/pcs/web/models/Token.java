package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 02.12.2021
 * Attestation02 - JWT + Mongo
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Document(collection = "blacklist")
public class Token {
    @Id
    private String _id;
    private String token;
}
