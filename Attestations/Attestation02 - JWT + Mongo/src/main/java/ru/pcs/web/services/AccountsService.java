package ru.pcs.web.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 02.12.2021
 * Attestation02 - JWT + Mongo
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public interface AccountsService {
    AccountDto addAccount(AccountDto account);
    List<AccountDto> getAccounts(int page, int size);
    Optional<Account> getAccountByEmail(String email);

}
