package ru.pcs.web.security.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.pcs.web.repositories.AccountsRepository;
import ru.pcs.web.security.JwtTokenManager;
import ru.pcs.web.security.filters.TokenAuthenticationFilter;
import ru.pcs.web.security.filters.TokenAuthorizationFilter;
import ru.pcs.web.security.filters.TokenLogoutFilter;
import ru.pcs.web.services.AccountsService;
import ru.pcs.web.services.BlackListTokensService;

/**
 * 02.12.2021
 * Attestation02 - JWT + Mongo
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    public static final String LOGIN_FILTER_PROCESSES_URL = "/login";
    public static final String LOGOUT_FILTER_PROCESSES_URL = "/logout";

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserDetailsService accountUserDetailsService;

    @Autowired
    private JwtTokenManager tokenManager;

    @Autowired
    private BlackListTokensService blackListTokensService;

    @Autowired
    private AccountsService accountsService;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(accountUserDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        TokenAuthenticationFilter tokenAuthenticationFilter = new TokenAuthenticationFilter(authenticationManagerBean(),
                objectMapper, tokenManager);
        tokenAuthenticationFilter.setFilterProcessesUrl(LOGIN_FILTER_PROCESSES_URL);

        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.addFilter(tokenAuthenticationFilter);
        http.addFilterBefore(new TokenAuthorizationFilter(objectMapper, tokenManager, blackListTokensService, accountsService),
                UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(new TokenLogoutFilter(blackListTokensService), TokenAuthenticationFilter.class);

        http.authorizeRequests()
                .antMatchers("/login/**").permitAll()
                .antMatchers("/logout/**").hasAnyAuthority();
    }
}
