package ru.pcs.web.security.filters;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.pcs.web.services.BlackListTokensService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 02.12.2021
 * Attestation02 - JWT + Mongo
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@RequiredArgsConstructor
public class TokenLogoutFilter extends OncePerRequestFilter {

    private final BlackListTokensService blackListTokensService;

    private final static RequestMatcher LOGOUT_REQUEST = new AntPathRequestMatcher("/api/logout", "GET");

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (LOGOUT_REQUEST.matches(request)) {
            String tokenHeader = request.getHeader("Authorization");
            if (tokenHeader != null && tokenHeader.startsWith("Bearer ")) {
                String token = tokenHeader.substring("Bearer ".length());
                // при выполнении логаута токен попадает в блэк-лист
                blackListTokensService.addToken(token);
                SecurityContextHolder.getContext();
                return;
            }
        }
        filterChain.doFilter(request, response);
    }
}
