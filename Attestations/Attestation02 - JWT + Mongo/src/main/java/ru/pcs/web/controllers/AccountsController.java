package ru.pcs.web.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.dto.AccountDto;
import ru.pcs.web.dto.AccountsResponse;
import ru.pcs.web.services.AccountsService;

import java.util.Collections;

/**
 * 02.12.2021
 * Attestation02 - JWT + Mongo
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountsController {

    private final AccountsService accountsService;

    @GetMapping
    public ResponseEntity<AccountsResponse> getAccounts(@RequestParam("page") int page, @RequestParam("size") int size) {
        return ResponseEntity.ok()
                .body(
                        AccountsResponse.builder()
                                .data(accountsService.getAccounts(page, size))
                                .build());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<AccountsResponse> addAccount(@RequestBody AccountDto account) {
        return ResponseEntity
                .status(201)
                .body(
                        AccountsResponse.builder()
                                .data(Collections.singletonList(accountsService.addAccount(account)))
                                .build());
    }

}
