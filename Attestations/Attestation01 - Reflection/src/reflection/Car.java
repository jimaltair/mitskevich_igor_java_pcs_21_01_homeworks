package reflection;

import java.time.LocalDate;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * 23.09.2021
 * Attestation01 - Reflection
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Car {

    private String model;
    private LocalDate dateOfProduction;
    private int mileage;
    private double engineVolume;
    private boolean isDiesel;

    public Car(String model, LocalDate dateOfProduction, int mileage, double engineVolume, boolean isDiesel) {
        this.model = model;
        this.dateOfProduction = dateOfProduction;
        this.mileage = mileage;
        this.engineVolume = engineVolume;
        this.isDiesel = isDiesel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (mileage != car.mileage) return false;
        if (Double.compare(car.engineVolume, engineVolume) != 0) return false;
        if (isDiesel != car.isDiesel) return false;
        if (!Objects.equals(model, car.model)) return false;
        return Objects.equals(dateOfProduction, car.dateOfProduction);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = model != null ? model.hashCode() : 0;
        result = 31 * result + (dateOfProduction != null ? dateOfProduction.hashCode() : 0);
        result = 31 * result + mileage;
        temp = Double.doubleToLongBits(engineVolume);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (isDiesel ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Car.class.getSimpleName() + "[", "]")
                .add("model='" + model + "'")
                .add("dateOfProduction=" + dateOfProduction)
                .add("mileage=" + mileage)
                .add("engineVolume=" + engineVolume)
                .add("isDiesel=" + isDiesel)
                .toString();
    }
}
