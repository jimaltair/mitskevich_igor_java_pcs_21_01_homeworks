package reflection;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

/**
 * 23.09.2021
 * Attestation01 - Reflection
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class Reflection {

    public static void cleanup(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput) {
        if (object instanceof Map) {
            cleanupMap((Map) object, fieldsToCleanup, fieldsToOutput);
        } else {
            cleanupObject(object, fieldsToCleanup, fieldsToOutput);
        }
    }

    private static void cleanupMap(Map map, Set<String> fieldsToCleanup, Set<String> fieldsToOutput) {

        boolean haveFieldsToCleanup = fieldsToCleanup.stream().anyMatch(map::containsKey);
        boolean haveFieldsToOutput = fieldsToOutput.stream().anyMatch(map::containsKey);

        if (haveFieldsToOutput || haveFieldsToCleanup) {
            fieldsToOutput.stream()
                    .filter(map::containsKey)
                    .forEach(field -> System.out.println(map.get(field)));
            map.keySet().removeAll(fieldsToCleanup);
        } else {
            throw new IllegalArgumentException("Объект не имеет полей, над которыми необходимо провести манипуляции");
        }
    }

    private static void cleanupObject(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput) {
        Class objectClass = object.getClass();
        Field[] fields = objectClass.getDeclaredFields();

        boolean haveFieldsToCleanup = false;
        boolean haveFieldsToOutput = false;

        for (Field field : fields) {
            field.setAccessible(true);
            if (fieldsToOutput.contains(field.getName())) {
                haveFieldsToOutput = true;
                try {
                    System.out.println(field.get(object));
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
            if (fieldsToCleanup.contains(field.getName())) {
                haveFieldsToCleanup = true;
                setVariableToDefaultValueOrNull(object, field);
            }
        }
        if(!haveFieldsToCleanup && !haveFieldsToOutput){
            throw new IllegalArgumentException("Объект не имеет полей, над которыми необходимо провести манипуляции");
        }
    }

    private static void setVariableToDefaultValueOrNull(Object object, Field field) {
        Class objectClass;
        try {
            objectClass = field.get(object).getClass();
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
        try {
            switch (objectClass.getName()) {
                case "java.lang.Byte":
                    field.set(object, (byte) 0);
                    break;
                case "java.lang.Short":
                    field.set(object, (short) 0);
                    break;
                case "java.lang.Integer":
                    field.set(object, 0);
                    break;
                case "java.lang.Long":
                    field.set(object, 0L);
                    break;
                case "java.lang.Float":
                    field.set(object, 0.0f);
                    break;
                case "java.lang.Double":
                    field.set(object, 0.0d);
                    break;
                case "java.lang.Boolean":
                    field.set(object, false);
                    break;
                case "java.lang.Character":
                    field.set(object, '\u0000');
                    break;
                default:
                    field.set(object, null);
            }
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
