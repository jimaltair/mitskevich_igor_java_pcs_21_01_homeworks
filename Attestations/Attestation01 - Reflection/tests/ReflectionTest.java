import org.junit.Assert;
import org.junit.Test;
import reflection.Car;
import reflection.Reflection;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 24.09.2021
 * Attestation01 - Reflection
 *
 * @author Mitskevich Igor
 * @version v1.0
 */
public class ReflectionTest {

    Car car = new Car("Volkswagen Passat", LocalDate.of(1991, 2, 25),
            352000, 2.0, true);

    Map<String, Integer> map = new HashMap<>();
    {
        map.put("count", 12);
        map.put("age", 38);
        map.put("size", 654);
        map.put("value", 124);
        map.put("capacity", -34);
    }

    /**
     * производим манипуляции над объектом Car
     * Set-ы содержат строки, которые совпадают с полями объекта
     */
    @Test
    public void cleanupTest1() {
        Set<String> fieldsToCleanup = Stream.of("name", "model", "age", "dateOfProduction",
                "engineVolume", "isDiesel").collect(Collectors.toSet());
        Set<String> fieldsToOutput = Stream.of("dateOfProduction", "age", "model", "xxxZZZ123",
                "engineVolume").collect(Collectors.toSet());

        Car expectedCar = new Car(null, null, 352000, 0.0, false);
        Reflection.cleanup(car, fieldsToCleanup, fieldsToOutput);

        Assert.assertEquals(expectedCar, car);
    }

    /**
     * производим манипуляции над объектом Car
     * Set-ы не содержат строк, совпадающих с полями объекта
     */
    @Test(expected = IllegalArgumentException.class)
    public void cleanupTest2() {
        Set<String> fieldsToCleanup1 = Stream.of("efg", "model234", "age", "dateOf@Production",
                "engine_Volume", "Diesel").collect(Collectors.toSet());
        Set<String> fieldsToOutput1 = Stream.of("12dateOfProduction12", "age", "wefg3", "xxxZZZ123",
                "engineVolume0").collect(Collectors.toSet());
        Reflection.cleanup(car, fieldsToCleanup1, fieldsToOutput1);
    }

    /**
     * производим манипуляции над объектом реализации интерфейса Map
     * Set-ы имеют пересечение с объектом
     */
    @Test
    public void cleanupTest3() {
        System.out.println("Исходная Map: " + map);

        Set<String> fieldsToCleanup2 = Stream.of("age", "value", "xxx358", "abcdrf").collect(Collectors.toSet());
        Set<String> fieldsToOutput2 = Stream.of("value", "size").collect(Collectors.toSet());

        Map<String, Integer> expectedMap = new HashMap<>();
        expectedMap.put("count", 12);
        expectedMap.put("size", 654);
        expectedMap.put("capacity", -34);

        Reflection.cleanup(map, fieldsToCleanup2, fieldsToOutput2);
        System.out.println("Map после манипуляций: " + expectedMap);

        Assert.assertEquals(expectedMap, map);
    }

    /**
     * производим манипуляции над объектом реализации интерфейса Map
     * Set-ы не имеют пересечений с объектом
     */
    @Test(expected = IllegalArgumentException.class)
    public void cleanupTest4() {
        Set<String> fieldsToCleanup3 = Stream.of("aerg", "vale2rue", "xxx358", "abcdrf").collect(Collectors.toSet());
        Set<String> fieldsToOutput3 = Stream.of("oijve", "@size").collect(Collectors.toSet());
        Reflection.cleanup(map, fieldsToCleanup3, fieldsToOutput3);
    }
}